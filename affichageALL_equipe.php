<!DOCTYPE html>
<html lang="fr">
  
  <head>
    <meta charset="utf-8" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.6.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="style.css" rel="stylesheet" type="text/css" />
    <title>Tournoi × Tournoi</title>

    <style>
        /* style du tableau liste de tournoi */
        #affichageTournoi {
          font-family: Arial, Helvetica, sans-serif;
          border-collapse: collapse;
          width: 85%;
        }

        #affichageTournoi td, #affichageTournoi th {
          border: 1px solid #ddd;
          padding: 8px;
        }

        #affichageTournoi tr {
          background-color:#f2f2f2;
        }

        #affichageTournoi tr:hover {background-color: #ddd;}

        #affichageTournoi th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #007bff;
        color: white;
        }
    </style>

  </head>

  <body>

        <center>

        <!-- affichage du tableau -->
        <table id="affichageTournoi" style="width:70%;">
        <tr>
            <th>Nom</th>
            <th>Nombre de joueur</th>
            <th>Nombre de compétion effectuée</th>
        </tr>

        <?php
          //connexion à la bdd
          $db_username = 'root';
          $db_password = '';
          $db_name     = 'gestiontournoi';
          $db_host     = 'gestiontournoi';
          $db = mysqli_connect($db_host, $db_username, $db_password,$db_name)
          or die('could not connect to database');

          $requete = "SELECT * FROM equipe";
          $exec_requete = mysqli_query($db,$requete);

          //Affichage des données via la méthode while($row)
          while($row = mysqli_fetch_array($exec_requete)){

            $idE=$row['id'];

            $requeteCount = "SELECT count(*) FROM equipejoueur WHERE idEquipe = '".$idE."'";
            $exec_requeteCount = mysqli_query($db,$requeteCount);
            $reponseCount      = mysqli_fetch_array($exec_requeteCount);
            $count        = $reponseCount['count(*)'];

            $requeteCount = "SELECT count(*) FROM equipejoueur WHERE idEquipe = '".$idE."' AND idJoueur = $idJoueur AND capitaine = 'O'";
            $exec_requeteCount = mysqli_query($db,$requeteCount);
            $reponseCount      = mysqli_fetch_array($exec_requeteCount);
            $countC        = $reponseCount['count(*)'];

            echo "<tr onclick=\"window.location='equipe_onclick.php?id=" .$row['id'] . "';\" style='cursor: pointer;'>";
            if ($countC != 0) {
              echo "<td>" . $row['nom'] . " *</td>";
            } else {
                echo "<td>" . $row['nom'] . " </td>";
              }
            echo "
            <td>" . $count . "</td>
            <td>" . $row['nbCompet'] . "</td></tr>"; 
          }
        ?>

        </table>

        </br></br>

        </center>
        
        <p style='margin-left:15%;color:white'>* équipes dont je suis le capitaine.</p>
        <br/><br/>

  </body>

</html>