<?php 
    require_once 'bdd/bdd.php';

    if($_SESSION['pseudonyme'] != ""){ //le header change si utilisateur connecté ou non
      $title = 'Tournoi × Tournoi';
      $page = 'accueil';
      include ('parts/header_logged.php');
    }
    else{
      $title = 'Tournoi × Tournoi';
      $page = 'accueil';
      include ('parts/header_unlog.php');
    }

    include ('parts/accueilcontent.php');

    include ('includes/footer.php');
    
?>