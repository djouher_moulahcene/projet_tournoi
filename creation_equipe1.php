<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <link href="style.css" rel="stylesheet" type="text/css" />
    <title>Création d'équipe</title>
</head>

<body>

    <?php 
        session_start();

        if(!isset($_SESSION['pseudonyme']))
            header('Location: connexion.php');
        else{
    ?>

        </br>
        <!-- formulaire de création de tournoi -->
        <div class="base" style="margin:auto">
        <form action="creation_equipe2.php" method="POST">
            <h1>Créer une Equipe</h1>
            <h1 style="font-size:20px;">Vous êtes le capitaine d'équipe</h1>

            <?php
            if( (isset($_SESSION['nomEquipe']) && isset($_SESSION['mail']) && isset($_SESSION['telephone']) && isset($_SESSION['nbJoueur'])) ){
                $nomEquipe = $_SESSION['nomEquipe'];
                $mail = $_SESSION['mail'];
                $tel = $_SESSION['telephone'];
                $nbJoueur = $_SESSION['nbJoueur'];
            }
            else {
                $nomEquipe = "";
                $mail = "";
                $tel = "";
                $nbJoueur = "";
            }
            ?>
                
            <label><b>Nom de l'équipe</b></label>
            <input type="text" minlength="1" maxlength="28" placeholder="Entrer le nom de l'équipe" name="nomEquipe" value="<?php echo $nomEquipe; ?>" required>

            <label><b>Mail</b></label>
            <input type="email" minlength="1" maxlength="48" placeholder="Entrer le mail de contact" name="mail" value="<?php echo $mail; ?>" required>

            <label><b>Téléphone</b></label>
            <input type="tel" pattern="^((\+\d{1,3}(-| )?\(?\d\)?(-| )?\d{1,5})|(\(?\d{2,6}\)?))(-| )?(\d{3,4})(-| )?(\d{4})(( x| ext)\d{1,5}){0,1}$" placeholder="Entrer le numéro de téléphone de contact" name="telephone" value="<?php echo $tel; ?>" required>

            <label><b>Nombre de joueur</b></label>
            <input type="number" min="1" max="32" placeholder="Entrer le nombre de joueur à inscrire" name="nbJoueur" value="<?php echo $nbJoueur; ?>" required>

            <input type="submit" id='submit' value='Créer une équipe'>

            <a href='page_equipe.php' style='color:black;'>← Retour</a></br>
        </form>
        </div>
        </br>

        <?php } ?>

</body>

</html>