<?php
    session_start();
    ini_set('display_errors', "stderr");error_reporting(E_ALL); 

    // connexion à la base de données
    $db_username = 'root';
    $db_password = '';
    $db_name     = 'gestiontournoi';
    $db_host     = 'gestiontournoi';
    $db = mysqli_connect($db_host, $db_username, $db_password,$db_name)
           or die('could not connect to database');

    $nomEquipe=$_SESSION['nomEquipe'];
    $mail=$_SESSION['mail'];
    $telephone=$_SESSION['telephone'];
    $nbJoueurs=$_SESSION['nbJoueur'];
    $idSession=$_SESSION['id'];

    $i=1;
    $bool=true;

    while($i<=$nbJoueurs-1){
        $chaineName = "joueurName";
        $chaineName2 = $chaineName."".$i;
        $joueurName=$_POST[$chaineName2];

        $chaine = "joueur";
        $chaine2 = $chaine."".$i;
        $joueur=$_POST[$chaine2];

        $_SESSION[$chaineName2] = $joueurName;

        if($joueur=="pseudo"){
            $requeteCount = "SELECT count(*) FROM utilisateur WHERE pseudonyme = '".$joueurName."'";
            $exec_requeteCount = mysqli_query($db,$requeteCount);
            $reponseCount      = mysqli_fetch_array($exec_requeteCount);
            $count        = $reponseCount['count(*)'];

                if($count!=0){
                    $requeteUtilisateur = "SELECT * FROM utilisateur WHERE pseudonyme = '".$joueurName."'";
                    $exec_requeteUtilisateur = mysqli_query($db,$requeteUtilisateur);
                    $row = mysqli_fetch_array($exec_requeteUtilisateur);
                    
                    $nom[$i] = $row['nom'];
                    $prenom[$i] = $row['prenom'];
                    $idUtilisateur[$i] = $row['id'];
                }
                else
                    $bool=false;
        }
        echo $_SESSION[$chaineName2];

        $i++;
    }

    if($bool){
        $requete = "INSERT INTO equipe VALUES (NULL,'$nomEquipe','$mail','$telephone',0,'$nbJoueurs',100)";
        $exec_requete = mysqli_query($db,$requete);
        $reponse      = mysqli_fetch_array($exec_requete);

        $requete_id = "SELECT id FROM equipe WHERE ID = (SELECT MAX(id) FROM equipe);";
        $exec_requete_id = mysqli_query($db,$requete_id);
        $reponse_id      = mysqli_fetch_array($exec_requete_id);
        $idEquipe              = $reponse_id['id'];

        $i=1;

        while($i<=$nbJoueurs-1){
            $chaineName = "joueurName";
            $chaineName2 = $chaineName."".$i;
            $joueurName=$_POST[$chaineName2];

            $chaine = "joueur";
            $chaine2 = $chaine."".$i;
            $joueur=$_POST[$chaine2];

            if($joueur=="manuellement"){
                $char = " ";
                $pos = strpos($joueurName, $char);
                $pos2 = $pos+1;

                $prenomM = substr($joueurName, $pos2);
                $nomM = substr($joueurName,0, $pos);

                $requete = "INSERT INTO joueur VALUES (NULL,'$nomM','$prenomM','1')";
                $exec_requete = mysqli_query($db,$requete);
                $reponse      = mysqli_fetch_array($exec_requete);

                $requete_id = "SELECT id FROM joueur WHERE ID = (SELECT MAX(id) FROM joueur);";
                $exec_requete_id = mysqli_query($db,$requete_id);
                $reponse_id      = mysqli_fetch_array($exec_requete_id);
                $id              = $reponse_id['id'];

                $idJoueur[$i]=$id;
            }
            else{

                $requeteCount = "SELECT count(*) FROM joueur WHERE idUtilisateur = '".$idUtilisateur[$i]."'";
                $exec_requeteCount = mysqli_query($db,$requeteCount);
                $reponseCount      = mysqli_fetch_array($exec_requeteCount);
                $count        = $reponseCount['count(*)'];

                if($count!=0){
                    $requeteUtilisateur = "SELECT * FROM joueur WHERE idUtilisateur = '".$idUtilisateur[$i]."'";
                    $exec_requeteUtilisateur = mysqli_query($db,$requeteUtilisateur);
                    $row = mysqli_fetch_array($exec_requeteUtilisateur);

                    $idJoueur[$i] = $row['id'];
                }
                else{
                    $requete = "INSERT INTO joueur VALUES (NULL,'$nom[$i]','$prenom[$i]','$idUtilisateur[$i]')";
                    $exec_requete = mysqli_query($db,$requete);

                    $requete_id = "SELECT id FROM joueur WHERE ID = (SELECT MAX(id) FROM joueur);";
                    $exec_requete_id = mysqli_query($db,$requete_id);
                    $reponse_id      = mysqli_fetch_array($exec_requete_id);
                    $id              = $reponse_id['id'];

                    $idJoueur[$i] = $id;
                }
            }

            $requete = "INSERT INTO equipejoueur VALUES ('$idEquipe','$idJoueur[$i]','N')";
            $exec_requete = mysqli_query($db,$requete);

            $i++;
        }

        $requeteCount = "SELECT count(*) FROM joueur WHERE idUtilisateur = '".$idSession."'";
        $exec_requeteCount = mysqli_query($db,$requeteCount);
        $reponseCount      = mysqli_fetch_array($exec_requeteCount);
        $count        = $reponseCount['count(*)'];

        if($count!=0){
            $requeteUtilisateur = "SELECT * FROM joueur WHERE idUtilisateur = '".$idSession."'";
            $exec_requeteUtilisateur = mysqli_query($db,$requeteUtilisateur);
            $row = mysqli_fetch_array($exec_requeteUtilisateur);

            $idCapitaine = $row['id'];
        }
        else{
            $nomCapitaine=$_SESSION['nom'];
            $prenomCapitaine=$_SESSION['prenom'];

            $requete = "INSERT INTO joueur VALUES (NULL,'$nomCapitaine','$prenomCapitaine','$idSession')";
            $exec_requete = mysqli_query($db,$requete);

            $requete_id = "SELECT id FROM joueur WHERE ID = (SELECT MAX(id) FROM joueur);";
            $exec_requete_id = mysqli_query($db,$requete_id);
            $reponse_id      = mysqli_fetch_array($exec_requete_id);
            $idCapitaine     = $reponse_id['id'];
        }

        $requete = "INSERT INTO equipejoueur VALUES ('$idEquipe','$idCapitaine','O')";
        $exec_requete = mysqli_query($db,$requete);

        unset($_SESSION['nomEquipe']);
        unset($_SESSION['mail']);
        unset($_SESSION['telephone']);
        unset($_SESSION['nbJoueur']);

        for ($i=1;$i<$nbJoueurs;$i++){
            $chaineName = "joueurName";
            $chaineName2 = $chaineName."".$i;

            unset($_SESSION[$chaineName2]);
        }

        header('Location:page_equipe.php?complete=0');
    }
    else
        header('Location: creation_equipe2.php?erreur=1');

    mysqli_close($db); // fermer la connexion
?>