  <body>

  <div style="width:65%;background-color:white;margin:auto;box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);">
    <center><b style="font-size:30px;" id="compteur"></b></center>
    <center><b style="font-size:35px;color:#007bff;"><?php echo $nom; ?></b></center>
  </div>

  </br>
  
  <div class="base" style="margin:auto;font-size: 1.2vmax">
  <?php
    echo '<div id="maDiv">';
    include ('arbre.php');
    echo '</div>';
  ?>
  </div>
  </div>

  </br>

  <?php 

    date_default_timezone_set('Europe/Paris');

    $requete = $bdd->prepare("SELECT nbEquipe FROM tournoi WHERE id = ? LIMIT 1");
    $requete->execute(array($idTournoi));
    $resultatNbEquipe = $requete->fetch();

    $nbEquipe = $resultatNbEquipe['nbEquipe'];

    $_SESSION['nbEquipe'] = $nbEquipe;

    $date = date('Y-m-d H:i');
  ?>

  <script>

    //script pour le timer
    dateDebutTournoi = new Date("<?php echo $dateDebut; ?>").getTime();
    dateFinTournoi = new Date("<?php echo $dateFin; ?>").getTime();

    function timer() {

    var now = new Date().getTime();

    var distance = dateDebutTournoi - now;
    var distanceF = dateFinTournoi - now;
    var text = "Début dans :";

    if(distance<0){
      distance=distanceF;
      text = "Fin dans :";
    }

    var jour = Math.floor(distance / (1000 * 60 * 60 * 24));
    var heure = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
    var minute = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
    var seconde = Math.floor((distance % (1000 * 60)) / 1000);

    document.getElementById("compteur").innerHTML = text + "</br>" + jour + "J " + heure + "H "
    + minute + "M " + seconde + "S </br>";

    if (distanceF<0) {
    clearInterval(x);
    document.getElementById("compteur").innerHTML = "TERMINÉ";
    }
    }

    timer();
    var x = setInterval(timer, 1000);

  </script>