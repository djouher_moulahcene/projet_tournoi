<div class="allButFooter">

  <center><b style='font-size:35px;color:white;'>Gérer les demandes de participations <a href='#' class='bulle' style='bottom:15px'><img src=images/infobulles.png style='width:20px;'><span>Ici vous pourrez accepter/refuser des équipes à rejoindre une compétition dont vous êtes gestionnaire</span> </a></b></center>

  </br>
  <!-- affichage du tableau -->
  <table id="affichageTournoi" style="width:70%;margin:auto">

  <tr>
      <th style="width:40%">Equipe</th>
      <th style="width:40%">Tournoi</th>
      <th id="vide"></th>
      <th id="valider" colspan=2>Accepter ?</th>
  </tr>

  <?php
    
    //connexion à la bdd
    $db_username = 'root';
    $db_password = '';
    $db_name     = 'gestiontournoi';
    $db_host     = 'gestiontournoi';
    $db = mysqli_connect($db_host, $db_username, $db_password,$db_name)
    or die('could not connect to database');

    //récupétation clé étrangère et selection des données associées
    $id = $_SESSION['id'];

    $requete = "SELECT * FROM tournoi,tournoiequipe WHERE idGestionnaire=$id AND tournoi.id = tournoiequipe.idTournoi";
    $exec_requete = mysqli_query($db,$requete);

    $i=0;

    //Affichage des données via la méthode while($row)
    while($row = mysqli_fetch_array($exec_requete)){

      if($row['Valide']=='N'){
      
      $idEquipe=$row['idEquipe'];
      $idTournoi=$row['idTournoi'];

      $requeteT = "SELECT * FROM equipe WHERE id=$idEquipe";
      $exec_requeteT = mysqli_query($db,$requeteT);
      $reponseT = mysqli_fetch_array($exec_requeteT);
      $nomEquipe = $reponseT['nom'];
      $mailEquipe = $reponseT['mail'];
      $telephoneEquipe = $reponseT['telephone'];
      $nbJoueurEquipe = $reponseT['nbJoueurs'];

      $requeteT = "SELECT * FROM tournoi WHERE id=$idTournoi";
      $exec_requeteT = mysqli_query($db,$requeteT);
      $reponseT = mysqli_fetch_array($exec_requeteT);
      $nomTournoi = $reponseT['nom'];

      echo "
      <tr>
      <td id='vide' style='background-color:#f2f2f2;border: 1px solid #ddd;'><a href=\"equipe_onclick.php?id=".$idEquipe."\" style='color:black'>$nomEquipe</a></td>
      
      <td id='vide' style='background-color:#f2f2f2;border: 1px solid #ddd;'><a href=\"tournoi_onclick.php?id=".$idTournoi."\" style='color:black'>$nomTournoi</a></td>
      <td id='vide'></td>
      <td id='valide' onclick=\"window.location='equipe_checked.php?idEquipe=".$idEquipe."&idTournoi=".$idTournoi."&valider=O';\">OUI</td>
      <td id='invalide' onclick=\"window.location='equipe_checked.php?idEquipe=".$idEquipe."&idTournoi=".$idTournoi."&valider=N';\">NON</td>
      </tr>";

        $i++;
      }
    }

    if($i==0){
      echo "<tr><td colspan=2 id='vide' style='background-color:#f2f2f2;border: 1px solid #ddd;'><center><em><b>Vous n'avez aucune équipe à valider</b></em></center></td><td id='vide'></td><td id='vide' style='background-color:#f2f2f2;text-align:center;border: 1px solid #ddd;'>/</td></tr>";
    }
  ?>

  </table>

</div>

<?php include 'includes/footer.php'; ?>

