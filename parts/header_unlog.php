  <?php
    $css = 'style.css';
    include 'includes/header.php'; 
  ?>
    <header>
    <!-- Header+menu (si session) -->
    <nav id="mainNavbar" class="navbar navbar-dark navbar-expand-lg py-0">
        <a href="accueil.php" class="navbar-brand <?php if($page=='accueil'){echo 'colorb';}?>">Tournoi × Tournoi</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navLinks" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navLinks">
            <ul class="navbar-nav mr-auto">

            </ul>

            <ul class="navbar-nav">
            <li class="nav-item <?php if($page=='accueil'){echo 'mt-sm-5 mt-lg-0';} ?>">
              <a class="nav-link" href="connexion.php">Connexion</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" href="inscription.php">Inscription</a>
            </li>
          </ul>
        </div>

    </nav>
    
    </header>

    </br>
