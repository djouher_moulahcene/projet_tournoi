<!-- division information tournoi -->
  <!-- les infos (idGestionnaire,id, date ect...) sont récupérer sur tournoi_click.php via l'id url -->
  <div class="box" style="width:65%;background-color:white;margin:auto;box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);padding:0;">
    
    <table class="inbox" id="infosTournoi">
      <tr>
        <td><b>Lieu : </b></td>
        <td><?php echo $lieu; ?></td>
      </tr>
      <tr>
        <td><b>Début : </b></td>
        <td><?php echo $dateD; ?></td>
      </tr>
      <tr>
        <td><b>Fin : </b></td>
        <td><?php echo $dateF; ?></td>
      </tr>
      <tr>
        <td><b>Nombre d'équipes : </b></td>
        <td><?php echo $nbEquipe; ?></td>
      </tr>
      <tr>
        <td><b>Type : </b></td>
        <td><?php echo $specificite; ?></td>
      </tr>
      <tr>
        <td><b>Durée : </b></td>
        <td><?php echo "$dureeHeure heure(s) $dureeMinute minute(s)"; ?></td>
      </tr>
      <tr>
        <td><b>Gestionnaire du tournoi : </b></td>
        <td><?php echo "$prenomGestionnaire $nomGestionnaire"; ?></td> <!-- Régler pseudo -->
      </tr>
    </table>

    <table class="inbox" id="infosTournoi">
        <th>Equipe participante</th>

          <?php

              $requete = "SELECT * FROM tournoiequipe WHERE idTournoi=$idTournoi AND valide='O'";
              $exec_requete = mysqli_query($db,$requete);

              $i=0;

              //Affichage des données via la méthode while($row)
              while($row = mysqli_fetch_array($exec_requete)){

                  $idEquipe = $row['idEquipe'];

                  $requeteT = "SELECT * FROM equipe WHERE id=$idEquipe";
                  $exec_requeteT = mysqli_query($db,$requeteT);
                  $reponseT = mysqli_fetch_array($exec_requeteT);

                  $nom = $reponseT['nom'];
                  
                  echo "<tr><td><a href=\"equipe_onclick.php?id=".$idEquipe."\" style='color:black'>$nom</a></tr></td>";

                  $i++;
              }

              if($i==0)
                echo "<tr><td><em>Aucune équipe inscrite</em></td></tr>";

          ?>

    </table>
  </div>

  </br>