<center>
  <div class="base">
  <form action="parts/confirmation_participation_tournoi.php" method="POST">
    
    <h1 style="text-align:center;">Participer à une compétition <a href='#' class='bulle' style='bottom:15px'><img src=images/infobulles.png style='width:20px;'><span>Choisissez dans le menu un tournoi auquel vous souhaitez participer (le gestionnaire du tournoi devra accepter votre équipe)</span> </a></h1>

    <div class="row">

    <div class="col-sm-10">
    <select name="choixTournoi" required>
    <option value="">--Choisissez une compétition--</option>

    <?php
        date_default_timezone_set('Europe/Paris');

        $db_username = 'root';
        $db_password = '';
        $db_name     = 'gestiontournoi';
        $db_host     = 'gestiontournoi';
        $db = mysqli_connect($db_host, $db_username, $db_password,$db_name)
        or die('could not connect to database');

        $requete = "SELECT * FROM tournoi";
        $exec_requete = mysqli_query($db,$requete);

        //récupération date et heure du jour
        $date = date('Y-m-d h:i:s');

        while($row = mysqli_fetch_array($exec_requete)){

          $nomTournoi=$row['nom'];
          $idTournoi=$row['id'];

          $dateDebut=$row['dateDebut'];
          $newDate = date("d/m/Y H:i", strtotime($dateDebut));

          if($date<$row['dateDebut']){
            $requeteCount = "SELECT count(*) FROM tournoiequipe WHERE idTournoi = '".$idTournoi."' AND idEquipe = '".$idEquipe."' ";
            $exec_requeteCount = mysqli_query($db,$requeteCount);
            $reponseCount      = mysqli_fetch_array($exec_requeteCount);
            $count             = $reponseCount['count(*)'];

            if($count==0)
              echo "<option value='$idTournoi'>$nomTournoi $newDate</option>";
          }

        }

    ?>

    </select>
    </div>

    <div class="col-sm-2">
    <input type="submit" id='submit' value='Participer'>
    </div>

    </div>

  </form>
  </div>

  </center>