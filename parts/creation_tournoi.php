<?php
  
  $css = '../style.css';
  $title = 'Tournoi × Tournoi';
  include '../includes/header.php';
  session_start(); 
?>
        </br>
        <!-- formulaire de création de tournoi -->
        <div style='width:65%;background-color:white;margin:auto;box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);padding:20px;'>
        <form action="../confirmation_tournoi.php" method="POST" style="width:100%;margin:auto;">
            <h1>Créer une compétition</h1>

            <?php
            if(isset($_GET['erreur'])){
                $err = $_GET['erreur'];
                if($err==1)
                    echo "<div style='background:#F0AFAF; border: 2px solid red;font-weight: bold;'><p style='text-align: center;margin-top:10px;'>Pseudonyme non-existant</p></div></br>";
                if($err==2)
                    echo "<div style='background:#F0AFAF; border: 2px solid red;font-weight: bold;'><p style='text-align: center;margin-top:10px;'>Le nombre d'équipe doit être une puissance de 2</p></div></br>";
                if($err==3)
                    echo "<div style='background:#F0AFAF; border: 2px solid red;font-weight: bold;'><p style='text-align: center;margin-top:10px;'>Incohérence des dates</p></div></br>";
            
            }

            if( (isset($_SESSION['nomTournoi']) && isset($_SESSION['lieu']) && isset($_SESSION['dateDebutTournoi']) && isset($_SESSION['heureDebutTournoi']) && isset($_SESSION['dateFinTournoi']) && isset($_SESSION['heureFinTournoi']) && isset($_SESSION['nbEquipe']) && isset($_SESSION['specificite']) && isset($_SESSION['pseudoGestionnaire'])) ){
                $nomTournoi = $_SESSION['nomTournoi'];
                $lieu = $_SESSION['lieu'];
                $dateDebutTournoi = $_SESSION['dateDebutTournoi'];
                $heureDebutTournoi = $_SESSION['heureDebutTournoi'];
                $dateFinTournoi = $_SESSION['dateFinTournoi'];
                $heureFinTournoi = $_SESSION['heureFinTournoi'];
                $nbEquipe = $_SESSION['nbEquipe'];
            }
            else {
                $nomTournoi = "";
                $lieu = "";
                $dateDebutTournoi = "";
                $heureDebutTournoi = "";
                $dateFinTournoi = "";
                $heureFinTournoi = "";
                $nbEquipe = "";

            }

            ?>
                
            <label><b>Nom</b></label>
            <input type="text" minlength="1" maxlength="48" placeholder="Entrer le nom du tournoi" name="nomTournoi" value="<?php echo $nomTournoi; ?>" required>

            <label><b>Lieu</b></label>
            <input type="text" minlength="1" maxlength="48" placeholder="Entrer le lieu du tournoi" name="lieu" value="<?php echo $lieu; ?>" required>

            <label><b>Date et heure de début</b></label>
            <input type="date" name="dateDebutTournoi" value="<?php echo $dateDebutTournoi; ?>" required>
            <input type="time" name="heureDebutTournoi" value="<?php echo $heureDebutTournoi; ?>" required>

            <label><b>Date et heure de fin</b></label>
            <input type="date" name="dateFinTournoi" value="<?php echo $dateFinTournoi; ?>" required>
            <input type="time" name="heureFinTournoi" value="<?php echo $heureFinTournoi; ?>" required>

            <label><b>Nombres d'équipes</b></label>
            <input type="number" min="2" max="32" placeholder="Entrer le nombre d'équipe à inscire" name="nbEquipe" value="<?php echo $nbEquipe; ?>" required>

            <label><b>Type de competition</b></label>
            <select name="specificite" required>
                <option value="C">Coupe</option>
                <option value="" style="color:gray;" disabled>Championnat --pas encore disponible--</option>
                <option value="" style="color:gray;" disabled>Tournoi --pas encore disponible--</option>
            </select>

            <label><b>Gestionnaire</b></label>
            <select name="pseudoGestionnaire" required>
                <option value="">--Choisissez un gestionnaire--</option>

                <?php
                    date_default_timezone_set('Europe/Paris');

                    $db_username = 'root';
                    $db_password = '';
                    $db_name     = 'gestiontournoi';
                    $db_host     = 'gestiontournoi';
                    $db = mysqli_connect($db_host, $db_username, $db_password,$db_name)
                    or die('could not connect to database');

                    $requete = "SELECT * FROM utilisateur WHERE id != 1 AND id != 2 AND id != 3";
                    $exec_requete = mysqli_query($db,$requete);

                    while($row = mysqli_fetch_array($exec_requete)){
                        if($row['id'] != 1 || $row['id'] != 2 || $row['id'] != 3){
                            $pseudo = $row['pseudonyme'];
                            echo "<option value='$pseudo'>$pseudo</option>";
                        }
                    }
                ?>

            </select>

            </br></br>

            <input type="submit" id='submit' value='Créer une compétition'>
            <a href=../tournoi.php style='color:black;'><- Retour</a></br>
        </form>
        </div>
        </br>
        <div style='width:65%;background-color:white;margin:auto;box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);padding:20px;'> <!-- AJOUTER -->
            <form action="./competition_aleatoire.php" method="POST" style="width:100%;margin:auto;">
                <input type='submit' id='submit' value='Générer une compétition aléatoire'>
            </form>
        </div>
        </br>
