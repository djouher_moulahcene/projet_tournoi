<div class="allButFooter">
<center>

<b style='font-size:35px;color:white;'>Compétition à gérer </b></br></br>

<!-- affichage du tableau -->
<div style="width:85%;overflow-x:auto;">
<table id="affichageTournoi">

  <tr>
      <th>Compétition</th>
      <th>Lieu</th>
      <th>Début</th>
      <th>Fin</th>
      <th>Équipe</th>
      <th>Status</th>
  </tr>

  <?php
    date_default_timezone_set('Europe/Paris');

    //récupétation clé étrangère et selection des données associées
    $id = $_SESSION['id'];

    if($_SESSION['admin']=="O"){  //Vérifie si admin ou non -> affiche tous les tournois ou non
      $WHERE = "";
      $gestionnaire = "Administrateur";
    }
    else{
      $WHERE = "WHERE idGestionnaire = $id";
      $gestionnaire = "Gestionnaire";
    }

    $requete = "SELECT * FROM tournoi $WHERE";
    $exec_requete = mysqli_query($db,$requete);

    //récupération date et heure du jour
    $date = date('Y-m-d h:i:s');

    $i = 0;

    //Affichage des données via la méthode while($row)
    while($row = mysqli_fetch_array($exec_requete)){
      echo "<tr onclick=\"window.location='tournoi_onclick.php?id=" .$row['id'] . "';\" style='cursor: pointer;'>
      <td>" . $row['nom'] . "</td>
      <td>" . $row['lieu'] . "</td>";

      $dateD=$row['dateDebut'];
      $dateD = date("d/m/Y h:i", strtotime($dateD));
      $dateF=$row['dateFin'];
      $dateF = date("d/m/Y h:i", strtotime($dateF));

      echo "
      <td>" . $dateD . "</td>
      <td>" . $dateF . "</td>";

      //affichage du statut
      if($date<$row['dateDebut'])
        $status="Non débuté";
      else if ($date<$row['dateFin'])
        $status="En cours";
      else
        $status="Terminé";

      echo
      "
      <td>$gestionnaire</td>
      <td>$status</td>
      </tr>";

        $i++;
    }

    if($i==0){
      echo "<tr><td colspan=6 id='vide' style='background-color:#f2f2f2;border: 1px solid #ddd;'><center><em><b>Vous n'avez aucune compétition</b></em></center></td></tr>";
    }

    echo "</table>
          </div>";

    $i=0;

    if($_SESSION['admin']=="O"){ //accès au formulaire creation_tounoi.php si utilisateur admin
      echo "</center><a href=parts/creation_tournoi.php style='margin-left:8%;'>+ Créer une compétition</a><center>";
    }

    if($_SESSION['admin']!="O"){
      $requete = "SELECT tournoi.id,tournoi.nom AS nomTournoi,equipe.nom AS nomEquipe,lieu,dateDebut,dateFin FROM joueur,equipejoueur,tournoiequipe,tournoi,equipe WHERE idUtilisateur = $id AND joueur.id = equipejoueur.idJoueur AND equipejoueur.idEquipe = tournoiequipe.idEquipe AND tournoiequipe.Valide = 'O' AND tournoiequipe.idTournoi = tournoi.id AND equipe.id = equipejoueur.idEquipe";
      $exec_requete = mysqli_query($db,$requete);

      echo "</br><b style='font-size:35px;color:white;'>Compétitions auxquelles vous participez</b></br></br>

      <div style='width:85%;overflow-x:auto;'>
      <table id='affichageTournoi' style='overflow-x: auto;'>
      <th>Compétition</th>
      <th>Lieu</th>
      <th>Début</th>
      <th>Fin</th>
      <th>Équipe</th>
      <th>Status</th>";

      while($row = mysqli_fetch_array($exec_requete)){
        echo "<tr onclick=\"window.location='tournoi_onclick.php?id=" .$row['id'] . "';\" style='cursor: pointer;'>
        <td>" . $row['nomTournoi'] . "</td>
        <td>" . $row['lieu'] . "</td>";

        $dateD=$row['dateDebut'];
        $dateD = date("d/m/Y h:i", strtotime($dateD));
        $dateF=$row['dateFin'];
        $dateF = date("d/m/Y h:i", strtotime($dateF));

        echo "
        <td>" . $dateD . "</td>
        <td>" . $dateF . "</td>";

        //affichage du statut
        if($date<$row['dateDebut'])
          $status="Non débuté";
        else if ($date<$row['dateFin'])
          $status="En cours";
        else
          $status="Terminé";

        echo
        "
        <td>" . $row['nomEquipe'] . "</td>
        <td>$status</td>
        </tr>";

        $i++;
      }
      if($i==0){
        echo "<tr><td colspan=6 id='vide' style='background-color:#f2f2f2;border: 1px solid #ddd;'><center><em><b>Vous n'avez aucune compétition</b></em></center></td></tr>";
      }
    }

  ?>

</table>
</div>
</br></br>
</div>