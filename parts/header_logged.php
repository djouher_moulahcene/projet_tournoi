  <?php
    $css = 'style.css';
    include 'includes/header.php'; 
  ?>

    <header>
    <!-- Header+menu (si session) -->
    <nav id="mainNavbar" class="navbar navbar-dark navbar-expand-lg py-0">
        <a href="accueil.php" class="navbar-brand <?php if($page=='accueil'){echo 'colorb';}?>">Tournoi × Tournoi</a>
        <button class="navbar-toggler" data-toggle="collapse" data-target="#navLinks" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navLinks">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item <?php if($page=='accueil'){echo 'mt-sm-5 mt-lg-0';} ?>">
                    <a href="page_equipe.php" class="nav-link <?php if($page=='page_equipe'){echo 'color';}?>">Équipes</a>
                </li>
                <li class="nav-item">
                    <a href="tournoi.php" class="nav-link <?php if($page=='tournoi'){echo 'color';}?>">Compétition</a>
                </li>
                <li class="nav-item">
                    <a href="espace_membre.php" class="nav-link <?php if($page=='espace_membre'){echo 'color';}?>">Espace membre</a>
                </li>
                <li class="nav-item">
                    <a href="validation_tournoi.php" class="nav-link <?php if($page=='validation_tournoi'){echo 'color';}?>">Valider une équipe</a>
                </li>
            </ul>

            <ul class="navbar-nav">
              <li class="nav-item">
                <p class="my-1">        
                  <?php 
                  $prenom = $_SESSION['prenom'];
                  $nom = $_SESSION['nom'];
                  echo "Bonjour $prenom";
                  ?>
                </p>
              </li>
          </ul>

          <ul class="navbar-nav">
            <li class="nav-item">
              <a class="nav-link" href="deconnexion.php" style="font-style: italic;">Deconnexion</a>
            </li>
          </ul>
        </div>

    </nav>
    </header>

    </br>
