<!DOCTYPE html>
<html>
  <head>
    <style>

.boite{width:65%;background-color:white;margin:auto;box-shadow: 0 0 20px 0 rgba(0, 0, 0, 0.2), 0 5px 5px 0 rgba(0, 0, 0, 0.24);}

/*
 *  Flex Layout Specifics
*/
#maDiv main{
  display:flex;
  margin-left: 10px;
  margin-right: 10px;
  flex-direction:row;
  justify-content: center;
}
#maDiv .round{
  display:flex;
  flex-direction:column;
  justify-content:center;
  width:370px;
  list-style:none;
  padding:0;
}
#maDiv .round .spacer{ flex-grow:1; }
#maDiv .round .spacer:first-child,
#maDiv .round .spacer:last-child{ flex-grow:.5; }

#maDiv .round .game-spacer{
    flex-grow:1;
  }

/*
 *  General Styles
*/
#maDiv body{
  font-family:sans-serif;
  font-size:small;
  padding:20px;
  line-height:2.8em;
}

#maDiv li.game{
  padding-left:20px;
}

#maDiv li.game.winner{
    font-weight:bold;
  }
  #maDiv li.game span{
    float:right;
    margin-right:5px;
  }

  #maDiv li.game-top{ border-bottom:3px solid #aaa; }

  #maDiv li.game-spacer{ 
    border-right:3px solid #aaa;
    min-height:70px;
  }

  #maDiv li.game-bottom{ 
    border-top:3px solid #aaa;
  }

  /* Arrière plan */
  #maDiv body {
  background-color: #4D545C;
  color: black;
}

        </style>

  </head>
  <body>
    <?php
        try {
        $bdd = new PDO("mysql:host=localhost;dbname=gestiontournoi;charset=utf8", "root", "");
        }
        catch (Exception $e) {
        die("Erreur : " . $e->getMessage());
        }
        $idTournoiActuel = $idTournoi;
        $requete = $bdd->prepare("SELECT nbEquipe FROM tournoi WHERE id = ? LIMIT 1");
        $requete->execute(array($idTournoiActuel));
        $resultatNbEquipe = $requete->fetch();
    ?>
    <?php if ($resultatNbEquipe['nbEquipe'] == 2): ?>
    <?php
        $requete = $bdd->prepare("SELECT idEquipe1, idEquipe2, score1, score2 FROM rencontre WHERE idTournoi = ? ORDER BY horaire");
        $requete->execute(array($idTournoiActuel));
        $i = 0;
        $tableauDonnees = array();
        while ($resultatRencontre = $requete->fetch()) {
            $tableauDonnees[$i] = $resultatRencontre['idEquipe1'];
            $i++;
            $tableauDonnees[$i] = $resultatRencontre['idEquipe2'];
            $i++;
            $tableauDonnees[$i] = $resultatRencontre['score1'];
            $i++;
            $tableauDonnees[$i] = $resultatRencontre['score2'];
            $i++;
        }
    ?>
    <div id="maDiv">
    <main id="tournament">
        <ul class="round round-1">
            <?php
                if($tableauDonnees[0]) {
                    if($tableauDonnees[2] > $tableauDonnees[3]) {
                        $idGagnant = $tableauDonnees[0];
                        $idPerdant = $tableauDonnees[1];
                        $scoreGagnant = $tableauDonnees[2];
                        $scorePerdant = $tableauDonnees[3];
                    }
                    else {
                        $idGagnant = $tableauDonnees[1];
                        $idPerdant = $tableauDonnees[0];
                        $scoreGagnant = $tableauDonnees[3];
                        $scorePerdant = $tableauDonnees[2];
                    }
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($idGagnant));
                    $resultatNomGagnant = $requete->fetch();
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($idPerdant));
                    $resultatNomPerdant = $requete->fetch();
                    if($tableauDonnees[2] && $tableauDonnees[3]) {
                        echo '
                            <li class="spacer">&nbsp;</li>
                            <li class="game game-top winner">' .$resultatNomGagnant["nom"] .'<span>' .$scoreGagnant . '</span></li>
                            <li class="game game-spacer">&nbsp;</li>
                            <li class="game game-bottom ">' .$resultatNomPerdant["nom"] .'<span>' .$scorePerdant . '</span></li><li class="spacer">&nbsp;</li>';
                    }
                    else {
                        echo '
                            <li class="spacer">&nbsp;</li>
                            <li class="game game-top ">' .$resultatNomGagnant["nom"] .'<span> <i></i> </span></li>
                            <li class="game game-spacer">&nbsp;</li>
                            <li class="game game-bottom ">' .$resultatNomPerdant["nom"] .'<span> <i></i> </span></li><li class="spacer">&nbsp;</li>';
                    }
                }
                else {
                    echo '
                        <li class="spacer">&nbsp;</li>
                        <li class="game game-top"> <i></i> <span> <i></i> </span></li>
                        <li class="game game-spacer">&nbsp;</li>
                        <li class="game game-bottom "> <i></i> <span> <i></i> </span></li><li class="spacer">&nbsp;</li>';
                }
            ?>
        </ul>
    </main>
    <?php elseif ($resultatNbEquipe['nbEquipe'] == 4): ?>
    <?php
        $requete = $bdd->prepare("SELECT idEquipe1, idEquipe2, score1, score2 FROM rencontre WHERE idTournoi = ? ORDER BY horaire");
        $requete->execute(array($idTournoiActuel));
        $i = 0;
        $tableauDonnees = array();
        while ($resultatRencontre = $requete->fetch()) {
            $tableauDonnees[$i] = $resultatRencontre['idEquipe1'];
            $i++;
            $tableauDonnees[$i] = $resultatRencontre['idEquipe2'];
            $i++;
            $tableauDonnees[$i] = $resultatRencontre['score1'];
            $i++;
            $tableauDonnees[$i] = $resultatRencontre['score2'];
            $i++;
        }
    ?>
    <main id="tournament">
        <ul class="round round-1">
            <?php
                $tableauGagnants = array();
                $j = 0;
                for($i = 0; $i < 8; $i = $i + 4) {
                    if(sizeof($tableauDonnees) > $i) { // si les équipes sont présentes
                        if(sizeof($tableauDonnees) > $i+2) { // si les scores sont présents
                            if($tableauDonnees[$i+2] > $tableauDonnees[$i+3]) { // si le score de l'équipe 1 est plus grands
                                $tableauGagnants[$j] = $tableauDonnees[$i];
                                $j++;
                                $tableauGagnants[$j] = $tableauDonnees[$i+2];
                                $j++;
                                $idGagnant = $tableauDonnees[$i];
                                $idPerdant = $tableauDonnees[$i+1];
                                $scoreGagnant = $tableauDonnees[$i+2];
                                $scorePerdant = $tableauDonnees[$i+3];
                            }
                            else {
                                $tableauGagnants[$j] = $tableauDonnees[$i+1];
                                $j++;
                                $tableauGagnants[$j] = $tableauDonnees[$i+3];
                                $j++;
                                $idGagnant = $tableauDonnees[$i+1];
                                $idPerdant = $tableauDonnees[$i];
                                $scoreGagnant = $tableauDonnees[$i+3];
                                $scorePerdant = $tableauDonnees[$i+2];
                            }
                            echo '<li class="spacer">&nbsp;</li><li class="game game-top winner">';
                            $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                            $requete->execute(array($idGagnant));
                            $resultatNom = $requete->fetch();
                            echo $resultatNom['nom'];
                            echo '<span>';
                            echo $scoreGagnant;
                            echo '</span></li><li class="game game-spacer">&nbsp;</li><li class="game game-bottom ">';
                            $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                            $requete->execute(array($idPerdant));
                            $resultatNom = $requete->fetch();
                            echo $resultatNom['nom'];
                            echo '<span>';
                            echo $scorePerdant;
                            echo '</span></li>';
                        }
                        else {
                            echo '<li class="spacer">&nbsp;</li><li class="game game-top">';
                            $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                            $requete->execute(array($tableauDonnees[$i]));
                            $resultatNom = $requete->fetch();
                            echo $resultatNom['nom'];
                            echo '<span>';
                            echo "<i></i>";
                            echo '</span></li><li class="game game-spacer">&nbsp;</li><li class="game game-bottom ">';
                            $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                            $requete->execute(array($tableauDonnees[$i+1]));
                            $resultatNom = $requete->fetch();
                            echo $resultatNom['nom'];
                            echo '<span>';
                            echo "<i></i>";
                            echo '</span></li>';
                        }
                    }
                    else {
                        echo '
                            <li class="spacer">&nbsp;</li>
                            <li class="game game-top"> <i></i> <span> <i></i> </span></li>
                            <li class="game game-spacer">&nbsp;</li>
                            <li class="game game-bottom "> <i></i> <span> <i></i> </span></li>';
                    }
                }
            ?>
            <li class="spacer">&nbsp;</li>
        </ul>
        <ul class="round round-2">
            <?php
                if(sizeof($tableauDonnees) > 8) { // si les équipes sont présentes
                    if($tableauDonnees[8] == $tableauGagnants[0]) {
                        $idHaut = $tableauDonnees[8];
                        $idBas = $tableauDonnees[9];
                        $scoreHaut = $tableauDonnees[10];
                        $scoreBas = $tableauDonnees[11];
                    }
                    else {
                        $idHaut = $tableauDonnees[9];
                        $idBas = $tableauDonnees[8];
                        $scoreHaut = $tableauDonnees[11];
                        $scoreBas = $tableauDonnees[10];
                    }
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($idHaut));
                    $resultatNomHaut = $requete->fetch();
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($idBas));
                    $resultatNomBas = $requete->fetch();
                    if($tableauDonnees[10] && $tableauDonnees[11]) { // si les scores sont présents
                        echo '<li class="spacer">&nbsp;</li>';
                        echo $scoreHaut > $scoreBas ? '<li class="game game-top winner">' : '<li class="game game-top">';
                        echo '' . $resultatNomHaut["nom"] . '<span>' . $scoreHaut . '</span></li><li class="game game-spacer">&nbsp;</li>';
                        echo $scoreHaut > $scoreBas ? '<li class="game game-bottom">' : '<li class="game game-bottom winner">';
                        echo '' . $resultatNomBas["nom"] . '<span>' . $scoreBas . '</span></li><li class="spacer">&nbsp;</li>';
                    }
                    else {
                        echo '
                        <li class="spacer">&nbsp;</li>
                        <li class="game game-top">' .$resultatNomHaut["nom"]. '<span><i></i></span></li>
                        <li class="game game-spacer">&nbsp;</li>
                        <li class="game game-bottom ">' .$resultatNomBas["nom"]. '<span > <i></i> </span></li>
                        <li class="spacer">&nbsp;</li>';
                    }
                }
                else {
                    echo '
                        <li class="spacer">&nbsp;</li>
                        <li class="game game-top"> <i></i> <span> <i></i> </span></li>
                        <li class="game game-spacer">&nbsp;</li>
                        <li class="game game-bottom "> <i></i> <span > <i></i> </span></li>
                        <li class="spacer">&nbsp;</li>';
                }
            ?>
        </ul>
    </main>
    <?php elseif ($resultatNbEquipe['nbEquipe'] == 8): ?>
    <?php
        $requete = $bdd->prepare("SELECT idEquipe1, idEquipe2, score1, score2 FROM rencontre WHERE idTournoi = ? ORDER BY horaire");
        $requete->execute(array($idTournoiActuel));
        $i = 0;
        $tableauDonnees = array();
        while ($resultatRencontre = $requete->fetch()) {
            $tableauDonnees[$i] = $resultatRencontre['idEquipe1'];
            $i++;
            $tableauDonnees[$i] = $resultatRencontre['idEquipe2'];
            $i++;
            $tableauDonnees[$i] = $resultatRencontre['score1'];
            $i++;
            $tableauDonnees[$i] = $resultatRencontre['score2'];
            $i++;
        }
    ?>
    <main id="tournament">
        <ul class="round round-1">
            <?php
                $tableauGagnants = array();
                $j = 0;
                for($i = 0; $i < 16; $i = $i + 4) {
                    if(sizeof($tableauDonnees) > $i) { // si les données sont présentes$tableauDonnees[$i]
                        if(sizeof($tableauDonnees) > $i+2) { // si les scores sont présents
                            if($tableauDonnees[$i+2] > $tableauDonnees[$i+3]) { // si le score de l'équipe 1 est plus grands
                                $tableauGagnants[$j] = $tableauDonnees[$i];// on met l'equipe gagnante
                                $j++;
                                $tableauGagnants[$j] = $tableauDonnees[$i+2];// on met le score de l'equipe gagnante
                                $j++;
                                $idGagnant = $tableauDonnees[$i];
                                $idPerdant = $tableauDonnees[$i+1];
                                $scoreGagnant = $tableauDonnees[$i+2];
                                $scorePerdant = $tableauDonnees[$i+3];
                            }
                            else {
                                $tableauGagnants[$j] = $tableauDonnees[$i+1];
                                $j++;
                                $tableauGagnants[$j] = $tableauDonnees[$i+3];
                                $j++;
                                $idGagnant = $tableauDonnees[$i+1];
                                $idPerdant = $tableauDonnees[$i];
                                $scoreGagnant = $tableauDonnees[$i+3];
                                $scorePerdant = $tableauDonnees[$i+2];
                            }
                            echo '<li class="spacer">&nbsp;</li><li class="game game-top winner">';
                            $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                            $requete->execute(array($idGagnant));
                            $resultatNom = $requete->fetch();
                            echo $resultatNom['nom'];
                            echo '<span>';
                            echo $scoreGagnant;
                            echo '</span></li><li class="game game-spacer">&nbsp;</li><li class="game game-bottom ">';
                            $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                            $requete->execute(array($idPerdant));
                            $resultatNom = $requete->fetch();
                            echo $resultatNom['nom'];
                            echo '<span>';
                            echo $scorePerdant;
                            echo '</span></li>';
                            
                        }
                        else {
                            echo '<li class="spacer">&nbsp;</li><li class="game game-top">';
                            $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                            $requete->execute(array($tableauDonnees[$i]));
                            $resultatNom = $requete->fetch();
                            echo $resultatNom['nom'];
                            echo '<span>';
                            echo "<i></i>";
                            echo '</span></li><li class="game game-spacer">&nbsp;</li><li class="game game-bottom ">';
                            $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                            $requete->execute(array($tableauDonnees[$i+1]));
                            $resultatNom = $requete->fetch();
                            echo $resultatNom['nom'];
                            echo '<span>';
                            echo "<i></i>";
                            echo '</span></li>';
                            
                        }
                    }
                    else {
                        echo '
                        <li class="spacer">&nbsp;</li>
                        <li class="game game-top"> <i></i> <span> <i></i> </span></li>
                        <li class="game game-spacer">&nbsp;</li>
                        <li class="game game-bottom "> <i></i> <span> <i></i> </span></li>';    
                    }
                }
                echo '<li class="spacer">&nbsp;</li>'; 
            ?>
        </ul>
        <ul class="round round-2">
            <?php
                if(sizeof($tableauDonnees) > 15) { // si les données sont présentes
                    for($i = 0; $i < 8; $i = $i + 2) {
                        if($tableauDonnees[16] == $tableauGagnants[$i]) {
                            ${"id" . $i} = $tableauGagnants[$i];
                            ${"score" . $i} = $tableauDonnees[18];
                        }
                        elseif($tableauDonnees[17] == $tableauGagnants[$i]) {
                            ${"id" . $i} = $tableauGagnants[$i];
                            ${"score" . $i} = $tableauDonnees[19];
                        }
                        elseif($tableauDonnees[20] == $tableauGagnants[$i]) {
                            ${"id" . $i} = $tableauGagnants[$i];
                            ${"score" . $i} = $tableauDonnees[22];
                        }
                        elseif($tableauDonnees[21] == $tableauGagnants[$i]) {
                            ${"id" . $i} = $tableauGagnants[$i];
                            ${"score" . $i} = $tableauDonnees[23];
                        }
                    }
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($id0));
                    $resultatNom0 = $requete->fetch();
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($id2));
                    $resultatNom2 = $requete->fetch();
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($id4));
                    $resultatNom4 = $requete->fetch();
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($id6));
                    $resultatNom6 = $requete->fetch();
                    if($tableauDonnees[18] && $tableauDonnees[19]) {
                        echo '<li class="spacer">&nbsp;</li>';
                        echo $score0 > $score2 ? '<li class="game game-top winner">' : '<li class="game game-top">';
                        echo $resultatNom0["nom"];
                        echo '<span>';
                        echo $score0;
                        echo '</span></li><li class="game game-spacer">&nbsp;</li>';
                        echo $score0 > $score2 ? '<li class="game game-bottom">' : '<li class="game game-bottom winner">';
                        echo $resultatNom2["nom"];
                        echo '<span>';
                        echo $score2;
                        echo '</span></li>';
                        if (sizeof($tableauDonnees) > 22) {
                            echo '<li class="spacer">&nbsp;</li>';
                            echo $score4 > $score6 ? '<li class="game game-top winner">' : '<li class="game game-top">';
                            echo $resultatNom4["nom"];
                            echo '<span>';
                            echo $score4;
                            echo '</span></li><li class="game game-spacer">&nbsp;</li>';
                            echo $score4 > $score6 ? '<li class="game game-bottom">' : '<li class="game game-bottom winner">';
                            echo $resultatNom6["nom"];
                            echo '<span>';
                            echo $score6;
                            echo '</span></li>';
                            echo '<li class="spacer">&nbsp;</li>';
                        }
                        else {
                            echo '
                                <li class="spacer">&nbsp;</li>
                                <li class="game game-top">' .$resultatNom4["nom"]. '<span><i></i></span></li>
                                <li class="game game-spacer">&nbsp;</li>
                                <li class="game game-bottom ">' .$resultatNom6["nom"]. '<span > <i></i> </span></li><li class="spacer">&nbsp;</li>';
                        }
                    }
                    else {
                        echo '
                            <li class="spacer">&nbsp;</li>
                            <li class="game game-top">' .$resultatNom0["nom"]. '<span><i></i></span></li>
                            <li class="game game-spacer">&nbsp;</li>
                            <li class="game game-bottom ">' .$resultatNom2["nom"]. '<span > <i></i> </span></li>';
                        echo '
                            <li class="spacer">&nbsp;</li>
                            <li class="game game-top">' .$resultatNom4["nom"]. '<span><i></i></span></li>
                            <li class="game game-spacer">&nbsp;</li>
                            <li class="game game-bottom ">' .$resultatNom6["nom"]. '<span > <i></i> </span></li><li class="spacer">&nbsp;</li>';
                    }
                }
                else {
                    for($i = 0; $i < 2; $i++) {
                        echo '
                            <li class="spacer">&nbsp;</li>
                            <li class="game game-top"> <i></i> <span> <i></i> </span></li>
                            <li class="game game-spacer">&nbsp;</li>
                            <li class="game game-bottom "> <i></i> <span> <i></i> </span></li>';
                    }
                    echo '<li class="spacer">&nbsp;</li>';
                }
            ?>
        </ul>
        <ul class="round round-3">
            <?php
                if(sizeof($tableauDonnees) > 23) {
                    if($tableauDonnees[24] == $id0 || $tableauDonnees[24] == $id2) {
                        $idHaut = $tableauDonnees[24];
                        $idBas = $tableauDonnees[25];
                        $scoreHaut = $tableauDonnees[26];
                        $scoreBas = $tableauDonnees[27];
                    }
                    else {
                        $idHaut = $tableauDonnees[25];
                        $idBas = $tableauDonnees[24];
                        $scoreHaut = $tableauDonnees[27];
                        $scoreBas = $tableauDonnees[26];
                    }
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($idHaut));
                    $resultatNomHaut = $requete->fetch();
                    $requete = $bdd->prepare("SELECT nom FROM equipe WHERE id = ? LIMIT 1");
                    $requete->execute(array($idBas));
                    $resultatNomBas = $requete->fetch();
                    if($tableauDonnees[26] && $tableauDonnees[27]) {
                        echo '
                            <li class="spacer">&nbsp;</li>';
                        echo $scoreHaut > $scoreBas ? '<li class="game game-top winner">' : '<li class="game game-top">';    
                        echo '' .$resultatNomHaut["nom"]. '<span>' .$scoreHaut. '</span></li>
                            <li class="game game-spacer">&nbsp;</li>';
                        echo $scoreHaut > $scoreBas ? '<li class="game game-bottom">' : '<li class="game game-bottom winner">';    
                        echo '' .$resultatNomBas["nom"]. '<span>' .$scoreBas. '</li><li class="spacer">&nbsp;</li>';
                    }
                    else {
                        echo '
                            <li class="spacer">&nbsp;</li>
                            <li class="game game-top">' .$resultatNomHaut["nom"]. '<span><i></i></span></li>
                            <li class="game game-spacer">&nbsp;</li>
                            <li class="game game-bottom ">' .$resultatNomBas["nom"]. '<span> <i></i> </span></li><li class="spacer">&nbsp;</li>';
                    }
                }
                else {
                    echo '
                        <li class="spacer">&nbsp;</li>
                        <li class="game game-top"> <i></i> <span> <i></i> </span></li>
                        <li class="game game-spacer">&nbsp;</li>
                        <li class="game game-bottom "> <i></i> <span> <i></i> </span></li><li class="spacer">&nbsp;</li>';
                }
            ?>
        </ul>
    </main>
            </div>
    <?php endif; ?>
  </body>
</html>
