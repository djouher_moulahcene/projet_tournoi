<?php
    require_once '../bdd/bdd.php';
    $idTournoi = $_GET['id'];
    $requete = $bdd->prepare("SELECT nbEquipe FROM tournoi WHERE id = ? LIMIT 1");
    $requete->execute(array($idTournoi));
    $nbEquipes = $requete->fetch();
    $requete = $bdd->prepare("SELECT dateDebut FROM tournoi WHERE id = ? LIMIT 1");
    $requete->execute(array($idTournoi));
    $horaire = $requete->fetch();
    $requete = $bdd->prepare("SELECT idEquipe1, idEquipe2, score1, score2 FROM rencontre WHERE idTournoi = ? ORDER BY horaire");
    $requete->execute(array($idTournoi));
    $i = 0;
    $rencontresResultats = array();
    while ($resultatRencontre = $requete->fetch()) { // Ce tableau contient les informations des rencontres.
        $rencontresResultats[$i] = $resultatRencontre['idEquipe1'];
        $i++;
        $rencontresResultats[$i] = $resultatRencontre['idEquipe2'];
        $i++;
        $rencontresResultats[$i] = $resultatRencontre['score1'];
        $i++;
        $rencontresResultats[$i] = $resultatRencontre['score2'];
        $i++;
    }
    $arrLength = count($rencontresResultats);
    // Premier cas : aucune rencontre n'est prévue
    if ($arrLength < 1) {
        $requete = "UPDATE tournoiequipe SET elimine = 'N' WHERE idTournoi = $idTournoi";
        $exec_requete = mysqli_query($db, $requete);
        $mareponse = $bdd->query("SELECT idEquipe FROM tournoiequipe WHERE idTournoi=$idTournoi AND Valide='O'");
        $equipesParticipantes = array();
        $i = 0;
        while ($mesdonnees = $mareponse->fetch()) { // Ce tableau contient l'identifiant des équipes qui participent.
            $equipesParticipantes[$i] = $mesdonnees['idEquipe'];
            $i = $i + 1;
          }
        $newdate =  date("Y-m-d H:i:s", strtotime('+1 days', strtotime($horaire['dateDebut'])));
        $value = rand(0,1) == 1;
        if ($value) {
            for ($j = 0; $j < $nbEquipes['nbEquipe'] / 2; $j++) {
                $k = $nbEquipes['nbEquipe'] - $j - 1;
                $bdd -> query("INSERT INTO rencontre VALUES ($idTournoi,'".$newdate."',$equipesParticipantes[$j],$equipesParticipantes[$k],NULL,NULL)");
            }
        }
        else {
            for ($j = 0; $j < $nbEquipes['nbEquipe'] - 1; $j = $j + 2) {
                $k = $j + 1;
                $bdd -> query("INSERT INTO rencontre VALUES ($idTournoi,'".$newdate."',$equipesParticipantes[$j],$equipesParticipantes[$k],NULL,NULL)");
            }
        }
    }
    // Deuxième cas : il y a les équipes de la première manche, mais PAS les scores de la première manche.
    elseif (($nbEquipes['nbEquipe'] == 4 && ! isset($rencontresResultats[7])) || $nbEquipes['nbEquipe'] == 2 || ($nbEquipes['nbEquipe'] == 8 && ! isset($rencontresResultats[15]))) {
        $mareponse = $bdd->query("SELECT idEquipe1, idEquipe2 FROM rencontre WHERE idTournoi = $idTournoi ORDER BY horaire");
        $equipesParticipantes = array();
        $i = 0;
        while ($mesdonnees = $mareponse->fetch()) { // Ce tableau contient l'identifiant des équipes qui participent.
            $j = $i + 1;
            $equipesParticipantes[$i] = $mesdonnees['idEquipe1'];
            $equipesParticipantes[$j] = $mesdonnees['idEquipe2'];
            $score1 = rand(0,100);
            $score2 = rand(0,100);
            while ($score1 == $score2)
                $score2 = rand(0,100);
            if ($score1 > $score2) {
                $requete1 = "UPDATE rencontre SET score1 = $score1, score2 = $score2 WHERE idTournoi = $idTournoi AND idEquipe1 = $equipesParticipantes[$i] AND idEquipe2 = $equipesParticipantes[$j]";
                $exec_requete1 = mysqli_query($db, $requete1);
                $requete1Bis = "UPDATE tournoiequipe SET elimine = 'N' WHERE idTournoi = $idTournoi AND idEquipe = $equipesParticipantes[$i]";
                $exec_requete1Bis = mysqli_query($db, $requete1Bis);
                $requete1BisBis = "UPDATE tournoiequipe SET elimine = 'O' WHERE idTournoi = $idTournoi AND idEquipe = $equipesParticipantes[$j]";
                $exec_requete1BisBis = mysqli_query($db, $requete1BisBis);
            }
            else {
                $requete2 = "UPDATE rencontre SET score1 = $score1, score2 = $score2 WHERE idTournoi = $idTournoi AND idEquipe1 = $equipesParticipantes[$i] AND idEquipe2 = $equipesParticipantes[$j]";
                $exec_requete2 = mysqli_query($db, $requete2);
                $requete2Bis = "UPDATE tournoiequipe SET elimine = 'N' WHERE idTournoi = $idTournoi AND idEquipe = $equipesParticipantes[$j]";
                $exec_requete2Bis = mysqli_query($db, $requete2Bis);
                $requete2BisBis = "UPDATE tournoiequipe SET elimine = 'O' WHERE idTournoi = $idTournoi AND idEquipe = $equipesParticipantes[$i]";
                $exec_requete2BisBis = mysqli_query($db, $requete2BisBis);
            }
            $i = $i + 2;
        }
    }
    // Troisième cas : il manque les scores de la deuxième manche.
    elseif ($nbEquipes['nbEquipe'] == 4 || ($nbEquipes['nbEquipe'] == 8 && ! isset($rencontresResultats[23]))) {
        if ($nbEquipes['nbEquipe'] == 4) {
            $reponse = $bdd->query("SELECT idEquipe FROM tournoiequipe WHERE idTournoi = $idTournoi AND elimine = 'N'");
            $equipesAyantGagne = array();
            $i = 0;
            while ($donnees = $reponse->fetch()) {
                $equipesAyantGagne[$i] = $donnees['idEquipe'];
                $i++;
            }
            echo $equipesAyantGagne[0];
            echo $equipesAyantGagne[1];
            $newdate =  date("Y-m-d H:i:s", strtotime('+2 days', strtotime($horaire['dateDebut'])));
            $score1 = rand(0,100);
            $score2 = rand(0,100);
            while ($score1 == $score2)
                $score2 = rand(0,100);
            $bdd -> query("INSERT INTO rencontre VALUES ($idTournoi,'".$newdate."',$equipesAyantGagne[0],$equipesAyantGagne[1],$score1,$score2)");
            if ($score1 > $score2) {
                $requete = "UPDATE tournoiequipe SET elimine = 'O' WHERE idTournoi = $idTournoi AND idEquipe = $equipesAyantGagne[1]";
                $exec_requete = mysqli_query($db, $requete);
            }
            else {
                $requete = "UPDATE tournoiequipe SET elimine = 'O' WHERE idTournoi = $idTournoi AND idEquipe = $equipesAyantGagne[0]";
                $exec_requete = mysqli_query($db, $requete);
            }
        }
        else { // Il y a 8 équipes
            $reponse = $bdd->query("SELECT idEquipe FROM tournoiequipe WHERE idTournoi = $idTournoi AND elimine = 'N'");
            $equipesAyantGagne = array();
            $i = 0;
            while ($donnees = $reponse->fetch()) { // Le tableau contient 4 entrées.
                $equipesAyantGagne[$i] = $donnees['idEquipe'];
                $i++;
            }
            $newdate =  date("Y-m-d H:i:s", strtotime('+2 days', strtotime($horaire['dateDebut'])));
            for ($i = 0; $i < 4; $i = $i + 2) {
                $j = $i + 1;
                $score1 = rand(0,100);
                $score2 = rand(0,100);
                while ($score1 == $score2)
                    $score2 = rand(0,100);
                $bdd -> query("INSERT INTO rencontre VALUES ($idTournoi,'".$newdate."',$equipesAyantGagne[$i],$equipesAyantGagne[$j],$score1,$score2)");
                if ($score1 > $score2) {
                    $requete = "UPDATE tournoiequipe SET elimine = 'O' WHERE idTournoi = $idTournoi AND idEquipe = $equipesAyantGagne[$j]";
                    $exec_requete = mysqli_query($db, $requete);
                }
                else {
                    $requete = "UPDATE tournoiequipe SET elimine = 'O' WHERE idTournoi = $idTournoi AND idEquipe = $equipesAyantGagne[$i]";
                    $exec_requete = mysqli_query($db, $requete);
                }
            }
        }
    }
    // Dernier cas : il y a 8 équipes et il manque la finale.
    else {
        $reponse = $bdd->query("SELECT idEquipe FROM tournoiequipe WHERE idTournoi = $idTournoi AND elimine = 'N'");
        $equipesAyantGagne = array();
        $i = 0;
        while ($donnees = $reponse->fetch()) { // Le tableau contient 2 entrées.
            $equipesAyantGagne[$i] = $donnees['idEquipe'];
            $i++;
        }
        $score1 = rand(0,100);
        $score2 = rand(0,100);
        while ($score1 == $score2)
            $score2 = rand(0,100);
        $newdate =  date("Y-m-d H:i:s", strtotime('+3 days', strtotime($horaire['dateDebut'])));
        $bdd -> query("INSERT INTO rencontre VALUES ($idTournoi,'".$newdate."',$equipesAyantGagne[0],$equipesAyantGagne[1],$score1,$score2)");
        if ($score1 > $score2) {
            $requete = "UPDATE tournoiequipe SET elimine = 'O' WHERE idTournoi = $idTournoi AND idEquipe = $equipesAyantGagne[1]";
            $exec_requete = mysqli_query($db, $requete);
        }
        else {
            $requete = "UPDATE tournoiequipe SET elimine = 'O' WHERE idTournoi = $idTournoi AND idEquipe = $equipesAyantGagne[0]";
            $exec_requete = mysqli_query($db, $requete);
        }
    }
    header("Location:../tournoi_onclick.php?id=".$idTournoi."&complete=3");
?>