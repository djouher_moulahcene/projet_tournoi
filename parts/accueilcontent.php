<?php require_once 'parts/accueildb.php'; ?>   

<div class="marquee d-none d-lg-block">
    <ul class="marquee-content mb-0 pl-0">

        <?php 
            $i = 0;
            while ($i < sizeof($datas)) {
                $data = $datas[$i];
                $idEq1 = $data['idEquipe1'];
                $idEq2 = $data['idEquipe2'];
                $sc1 = $data['score1'];
                $sc2 = $data['score2'];
                $idT = $data['idTournoi'];

                $reponse = $bdd->query ("SELECT nom FROM equipe WHERE id = '".$idEq1."'");
                $nomEq1 = $reponse->fetch();
                $nomEq1 = $nomEq1['nom'];

                $reponse = $bdd->query ("SELECT nom FROM equipe WHERE id = '".$idEq2."'");
                $nomEq2 = $reponse->fetch();
                $nomEq2 = $nomEq2['nom'];

                $reponse = $bdd->query ("SELECT nom FROM tournoi WHERE id = '".$idT."'");
                $nomT = $reponse->fetch();
                $nomT = $nomT['nom'];

                echo "
                    <li class='pr-0'>
                        <div class='container'>
                            <div class='row d-flex flex-column border-right'>
                                <div class='col-sm-12 px-1'>
                                    <p class='mb-0 text-center px-0'>$nomT</p>
                                    <hr class='my-0'>
                                </div>
                                <div class='col-sm-12'>
                                    <div class='row px-1'>
                                        <p class='col-sm-10 mb-0 px-0'>$nomEq1</p>
                                        <p class='col-sm-2 mb-0 text-right px-0'>$sc1</p>
                                    </div>
                                </div>
                                <div class='col-sm-12'>
                                    <div class='row px-1'>
                                        <p class='col-sm-10 mb-0 px-0'>$nomEq2</p>
                                        <p class='col-sm-2 mb-0 text-right px-0'>$sc2</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </li>
                ";
                $i++;
            }
        ?>
    </ul>


</div>

<div class="marquee2 d-none d-sm-block d-lg-none">
    <ul class="marquee-content2 mb-0 pl-0">

    <?php 
        $i = 0;
        while ($i < sizeof($datas)) {
            $data = $datas[$i];
            $idEq1 = $data['idEquipe1'];
            $idEq2 = $data['idEquipe2'];
            $sc1 = $data['score1'];
            $sc2 = $data['score2'];
            $idT = $data['idTournoi'];

            $reponse = $bdd->query ("SELECT nom FROM equipe WHERE id = '".$idEq1."'");
            $nomEq1 = $reponse->fetch();
            $nomEq1 = $nomEq1['nom'];

            $reponse = $bdd->query ("SELECT nom FROM equipe WHERE id = '".$idEq2."'");
            $nomEq2 = $reponse->fetch();
            $nomEq2 = $nomEq2['nom'];

            $reponse = $bdd->query ("SELECT nom FROM tournoi WHERE id = '".$idT."'");
            $nomT = $reponse->fetch();
            $nomT = $nomT['nom'];

            echo "
                <li class='pr-0'>
                    <div class='container'>
                        <div class='row d-flex flex-column border-right'>
                            <div class='col-sm-12 px-1'>
                                <p class='mb-0 text-center px-0'>$nomT</p>
                                <hr class='my-0'>
                            </div>
                            <div class='col-sm-12'>
                                <div class='row px-1'>
                                    <p class='col-sm-10 mb-0 px-0'>$nomEq1</p>
                                    <p class='col-sm-2 mb-0 text-right px-0'>$sc1</p>
                                </div>
                            </div>
                            <div class='col-sm-12'>
                                <div class='row px-1'>
                                    <p class='col-sm-10 mb-0 px-0'>$nomEq2</p>
                                    <p class='col-sm-2 mb-0 text-right px-0'>$sc2</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </li>
            ";
            $i++;
        }
    ?>
    </ul>
</div>


<section class="container mb-5" id="accueil">
    <div class="row">
        <div class="col-sm-6" id="mother1">
            <div class="row flex-column">
                <div class="col-sm-12 mb-5 mb-sm-0 mb-md-4 mr-4 text-center" id="first">
                    <p class="text-center">Créez votre tournoi</p>
                    <form action="tournoi.php">
                        <input class="btn" type="submit" value="Créez tournoi" />
                    </form>
                </div>
                <div class="col-sm-12 mr-4 d-none d-md-block" id="second"></div>
            </div>
        </div>
        <div class="col-sm-6" id="mother2">
            <div class="row flex-column">
                <div class="col-sm-12 mb-4 ml-4 d-none d-md-block" id="second"></div>
                <div class="col-sm-12 ml-sm-4 mt-5 mt-sm-0 text-center" id="fourth">
                    <p class="text-center">Créez votre equipe</p>
                    <form action="page_equipe.php">
                        <input class="btn" type="submit" value="Créez equipe" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    <br class="d-sm-none">
    <br class="d-sm-none">
</section>



<script>
    const root = document.documentElement;
    const marqueeElementsDisplayed = getComputedStyle(root).getPropertyValue("--marquee-elements-displayed");
    const marqueeContent = document.querySelector("ul.marquee-content");

    root.style.setProperty("--marquee-elements", marqueeContent.children.length);

    for(let i=0; i<marqueeElementsDisplayed; i++) {
        marqueeContent.appendChild(marqueeContent.children[i].cloneNode(true));
    }

    
    const marqueeElementsDisplayed2 = getComputedStyle(root).getPropertyValue("--marquee-elements-displayed2");
    const marqueeContent2 = document.querySelector("ul.marquee-content2");
    root.style.setProperty("--marquee-elements2", marqueeContent2.children.length);
    for(let i=0; i<marqueeElementsDisplayed2; i++) {
        marqueeContent2.appendChild(marqueeContent2.children[i].cloneNode(true));
    }
</script>