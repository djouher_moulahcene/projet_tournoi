<?php
    $db_username = 'root';
    $db_password = '';
    $db_name     = 'gestiontournoi';
    $db_host     = 'gestiontournoi';
    $db = mysqli_connect($db_host, $db_username, $db_password,$db_name)
        or die('could not connect to database');
    function nomAleatoire() {
        $determinants = array(
            'Tigres',
            'Doyens',
            'Capitaines',
            'Ours',
            'Seigneurs',
            'Pauvres',
            'Viseurs',
            'Troubadours',
            'Danseurs',
            'Artistes',
            'Coureurs',
            'Voyageurs',
            'Lanceurs',
            'Camarades',
            'Acolytes',
            'Partenaire',
            'Amoureux',
            'Sportifs',
            'Villageois',
        );
        $complements = array(
            'fiers',
            'en voyage',
            'de passage',
            'rapides',
            'millionaires',
            'en patrouille',
            'radins',
            'de la vieille',
            'sans parole',
            'bons vivants',
            'impassibles',
            'audacieux',
            'aventureux',
            'joyeux',
            'solitaires',
            'en combat',
            'altruistes',
            'charitables',
            'galants',
            'philanthropes',
            'fraternel',
            'sympathique',
            'chaleureux',
        );
        $nom = 'Les ' . $determinants[rand(0,18)] . ' ' . $complements[rand(0,22)];
        return $nom;
    }
    function nbEquipeAleatoire() {
        $tmp = rand(0,2);
        if ($tmp == 0)
            return 2;
        if ($tmp == 1)
            return 4;
        if ($tmp == 2)
            return 8;
    }
    $nom = nomAleatoire();
    $lieu = 'Montpellier';
    $nbEquipe = nbEquipeAleatoire();
    $specificite = 'C';
    $idGestionnaire = 10;
    $heureActuelle = date("Y-m-d H:i:s");
    $heureTournoi = new DateTime($heureActuelle);
    $heureTournoi->modify('-1 days');
    $heureTournoiString = $heureTournoi->format('Y-m-d H:i:s');
    $heureFin = "2021-06-20 00:00:00.0";
    $nbSecondesEntre = strtotime($heureFin) - time();
    $nbHeure = (int)($nbSecondesEntre / 3600);
    $nbMinute = $nbSecondesEntre % 3600;
    $requete = "INSERT INTO tournoi VALUES (NULL,'".$nom."', '".$lieu."', '".$heureTournoiString."', '".$heureFin."', '".$nbHeure."', '".$nbMinute."', '".$nbEquipe."', '".$specificite."', '".$idGestionnaire."')";
    $exec_requete = mysqli_query($db,$requete);
    $requeteIdTournoi = "SELECT id FROM tournoi WHERE id = (SELECT MAX(id) FROM tournoi);";
    $exec_requeteIdTournoi = mysqli_query($db,$requeteIdTournoi);
    $reponse_id = mysqli_fetch_array($exec_requeteIdTournoi);
    $idT = $reponse_id['id'];
    $mail = '';
    $telephone = '0000000000';
    $nbCompetition = 1;
    $nbJoueur = 1;
    $niveau = 100;
    for ($i = 0; $i < $nbEquipe; $i++) {
        $nomEquipe = nomAleatoire();
        $requete = "INSERT INTO equipe VALUES (NULL, '".$nomEquipe."', '".$mail."', '".$telephone."', $nbCompetition, $nbJoueur, $niveau)";
        $exec_requete = mysqli_query($db,$requete);
        $requeteIdEquipe = "SELECT id FROM equipe WHERE id = (SELECT MAX(id) FROM equipe);";
        $exec_requeteIdEquipe = mysqli_query($db,$requeteIdEquipe);
        $reponse_id = mysqli_fetch_array($exec_requeteIdEquipe);
        $idE = $reponse_id['id'];
        $requeteBis = "INSERT INTO tournoiequipe VALUES ('".$idT."', '".$idE."', 'O', 'N')";
        $exec_requeteBis = mysqli_query($db,$requeteBis);
    }
    header('Location: ../tournoi.php?complete=0');
?>
