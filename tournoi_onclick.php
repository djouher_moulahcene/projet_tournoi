<?php 

    require_once 'bdd/bdd.php';

    if($_SESSION['pseudonyme'] != ""){ //vérification si utilisateur connecté
        include ('parts/header_logged.php');

        echo '</br></br>';

        //bouton retour arrière vers la liste des compétitions
        echo "<center><table style='width:65%;'><tr><td><a href='tournoi.php'>← liste des compétitions</a></td></tr></table></center>";

        //récupération de l'id du tournoi via l'id url
        $idTournoi=$_GET['id'];

        //A CHANGER ICI =====> RECUPERER DONNE SSI CLE ETRANGERE CORRECT
        
        //Vérifitcation si clé étrangère correct
        $idUtilisateur=$_SESSION['id'];
        $admin=$_SESSION['admin'];

        //SELECT * FROM utilisateur,tournoi WHERE utilisateur.id=9 AND idGestionnaire = utilisateur.id AND tournoi.id = 19

        //$requete1 = "SELECT count(*) FROM joueur,equipejoueur,tournoiequipe,tournoi,equipe WHERE idUtilisateur = $idUtilisateur AND joueur.id = equipejoueur.idJoueur AND equipejoueur.idEquipe = tournoiequipe.idEquipe AND tournoiequipe.Valide = 'O' AND tournoiequipe.idTournoi = tournoi.id AND equipe.id = equipejoueur.idEquipe AND tournoi.id = $idTournoi";
        
        $reponse = $bdd->query( "SELECT count(*) FROM joueur,equipejoueur,tournoiequipe,tournoi,equipe WHERE idUtilisateur = $idUtilisateur AND joueur.id = equipejoueur.idJoueur AND equipejoueur.idEquipe = tournoiequipe.idEquipe AND tournoiequipe.Valide = 'O' AND tournoiequipe.idTournoi = tournoi.id AND equipe.id = equipejoueur.idEquipe AND tournoi.id = $idTournoi" );
        $countArray = $reponse->fetch();
        $count = $countArray['count(*)'];

        if($count==0){
            $reponse = $bdd->query( "SELECT count(*) FROM utilisateur,tournoi WHERE utilisateur.id=$idUtilisateur AND idGestionnaire = utilisateur.id AND tournoi.id = $idTournoi" );
            $countArray = $reponse->fetch();
            $count = $countArray['count(*)'];
        }
        
        //si corecct affichage infos compet
        if($count!=0 || $admin=="O"){
            //récupération de toutes les données tournoi via clé primaire (idTournoi)
            $requete = "SELECT * FROM tournoi WHERE id = '".$idTournoi."'";
            $exec_requete = mysqli_query($db,$requete);
            $row = mysqli_fetch_array($exec_requete);

            $nom=$row['nom'];
            $lieu=$row['lieu'];
            $dateDebut=$row['dateDebut'];
            $dateFin=$row['dateFin'];
            $nbEquipe=$row['nbEquipe'];
            $dureeHeure=$row['dureeHeure'];
            $dureeMinute=$row['dureeMinute'];
            $idGestionnaire=$row['idGestionnaire'];

            $requetePseudo = "SELECT * FROM utilisateur WHERE id = '".$idGestionnaire."'";
            $exec_requetePseudo = mysqli_query($db,$requetePseudo);
            $reponsePseudo      = mysqli_fetch_array($exec_requetePseudo);
            $prenomGestionnaire  = $reponsePseudo['prenom'];
            $nomGestionnaire  = $reponsePseudo['nom'];

            //T = Tournoi ; H = Championnat ; C = Coupe
            if ($row['specificite']=='T')
                $specificite='Tournoi';
            else if ($row['specificite']=='H')
                $specificite='Championnat';
            else
                $specificite='Coupe';

            include ('parts/fiche_tournoi.php');

            $requeteVainqueur = "SELECT count(*) FROM tournoiequipe WHERE idTournoi = $idTournoi AND elimine = 'N'";
            $exec_requeteVainqueur = mysqli_query($db,$requeteVainqueur);
            $reponseVainqueur      = mysqli_fetch_array($exec_requeteVainqueur);
            $vainqueur  = $reponseVainqueur['count(*)'];

            if($vainqueur == 1)
                include ('parts/vainqueur.php');
            
            elseif ($idUtilisateur==$idGestionnaire && ( ($date<$dateFin && $date>$dateDebut) || ($date<$dateFin && $vainqueur==($nbEquipe/2)) ) ){
                include ('parts/rencontre_aleatoire.php');
                echo "</br>";
            }

            echo "<div id='confirmation'></div>";
            if($idUtilisateur==$idGestionnaire && $date<$dateFin)
                include ('parts/rencontre.php');

            $dateD = date("d/m/Y h:i", strtotime($dateDebut));
            $dateF = date("d/m/Y h:i", strtotime($dateFin));

            include ('parts/info_tournoi.php');

            if(isset($_GET['complete'])){
                $complete = $_GET['complete'];
                if($complete==1){
                $message='Rencontres enregistrées';
                echo '<script type="text/javascript">window.alert("'.$message.'");</script>';
                }
                elseif($complete==2){
                $message='Scores enregistrés';
                echo '<script type="text/javascript">window.alert("'.$message.'");</script>';
                }
                elseif($complete==3){
                    $message='Rencontres définies';
                    echo '<script type="text/javascript">window.alert("'.$message.'");</script>'; // J'AI CHANGÉ ÇA
                }
            }
            include ('includes/footer.php');
        }
        else
            header('Location: tournoi.php'); //si utilisateur non admin/gestionnaire du tournoi alors pas accès
    }
    else
        header('Location: connexion.php');
?>
