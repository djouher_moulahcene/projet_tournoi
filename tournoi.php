<?php 

    require_once 'bdd/bdd.php';

    if($_SESSION['pseudonyme'] != ""){ //vérification si utilisateur connécté
      $title = 'Competiton';
      $page = 'tournoi';
      include ('parts/header_logged.php');

      echo '</br></br>';

      if(isset($_GET['complete'])){ //message si un tournoi vient d'être créé
        $com = $_GET['complete'];
        if($com==0)
        echo "<center><table style='width:70%;background-color:#C5F7CA;border:1px solid green;text-align:center;'><tr><td><b>Compétition créée</b></td></tr></table></center>";
        }

        include ('parts/affichage_tournoi.php'); //affichage liste des tournois de l'utilisateur

        include ('includes/footer.php');
    }
    else{
        header('Location: connexion.php');
    }
?>