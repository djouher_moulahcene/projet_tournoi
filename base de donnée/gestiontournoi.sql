-- phpMyAdmin SQL Dump
-- version 4.9.5deb2
-- https://www.phpmyadmin.net/
--
-- Hôte : localhost:3306
-- Généré le : sam. 29 mai 2021 à 18:50
-- Version du serveur :  8.0.25-0ubuntu0.20.04.1
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `gestiontournoi`
--

-- --------------------------------------------------------

--
-- Structure de la table `equipe`
--

CREATE TABLE `equipe` (
  `id` smallint UNSIGNED NOT NULL,
  `nom` varchar(30) NOT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `telephone` varchar(20) DEFAULT NULL,
  `nbCompet` tinyint UNSIGNED DEFAULT '0',
  `nbJoueurs` int NOT NULL,
  `niveau` tinyint UNSIGNED NOT NULL DEFAULT '100'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `equipe`
--

INSERT INTO `equipe` (`id`, `nom`, `mail`, `telephone`, `nbCompet`, `nbJoueurs`, `niveau`) VALUES
(1, 'Damiers jaunes', 'damiers-jaunes@pm.me', '0763333333', 1, 2, 100),
(2, 'Viser juste', 'viser-juste@pm.me', '0763333333', 1, 2, 100),
(3, 'Les cochonnets', 'les-cochonnets@pm.me', '0763517392', 1, 2, 100),
(4, 'Un jour, une victoire', 'jour-victoire@pm.me', '0763919291', 3, 2, 100),
(5, 'Jamais sans ma boule', 'jamais-boule@etu.umontpellier.fr', '0873777172', 3, 2, 100),
(6, 'Les Courageux', 'les-courageux@gmail.com', '0973336182', 1, 2, 100),
(7, 'Les Ours de la Montagne', 'ours-courageux@gmail.com', '0666663516', 2, 2, 100),
(8, 'Les Camarades du dimanche', 'camarades-dimanche@gmail.com', '071728271', 2, 2, 100),
(9, 'Bonne Pression', 'bonne-pression@pm.me', '0666556666', 1, 2, 100),
(10, 'Vision de Nuit', 'vision-de-nuit@laposte.net', '0666557372', 1, 2, 100),
(11, 'BonneVision', 'bonne-vision@pm.me', '0666666666', 0, 2, 100),
(12, 'Sans mauvais jeu de mot', 'jdm@laposte.net', '0766554178', 2, 2, 100),
(13, 'La Boule en Feu', 'lbef@hotmail.fr', '0877776178', 1, 2, 100),
(14, 'On est ici', 'on-est-ici@tutanota.com', '066651628', 1, 2, 100),
(15, 'Les amis', 'les-amis@montpellier.fr', '066666666', 2, 2, 100),
(16, 'De Marbre', 'marbre@montpellier.fr', '066666666', 1, 1, 100),
(17, 'Herbre verte', 'herbe@montpellier.fr', '07666666666', 1, 2, 100),
(18, 'Toujours devant', 'devant@montpellier.fr', '066666666', 1, 2, 100),
(19, 'Alfred', 'alfred@debian.org', '066666666', 1, 2, 100),
(20, 'Auvergne', 'perso@auverge.fr', '0666666666', 1, 2, 100),
(21, 'Dame et Pique', 'dp@yopmail.fr', '0666666666', 1, 2, 100),
(22, 'Les vrais Irlandais', 'mexique@fastmail.com', '0666666666', 1, 2, 100),
(23, 'Carlos Fan Club', 'carlos@hotmail.fr', '0666666666', 1, 2, 100);

-- --------------------------------------------------------

--
-- Structure de la table `equipejoueur`
--

CREATE TABLE `equipejoueur` (
  `idEquipe` smallint UNSIGNED NOT NULL,
  `idJoueur` int UNSIGNED NOT NULL,
  `capitaine` char(1) DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `equipejoueur`
--

INSERT INTO `equipejoueur` (`idEquipe`, `idJoueur`, `capitaine`) VALUES
(1, 1, 'N'),
(1, 2, 'O'),
(2, 3, 'N'),
(2, 4, 'O'),
(3, 5, 'N'),
(3, 6, 'O'),
(4, 7, 'N'),
(4, 8, 'O'),
(5, 9, 'N'),
(5, 10, 'O'),
(6, 11, 'N'),
(6, 12, 'O'),
(7, 15, 'N'),
(7, 16, 'O'),
(8, 17, 'N'),
(8, 18, 'O'),
(9, 12, 'N'),
(9, 19, 'O'),
(10, 6, 'N'),
(10, 20, 'O'),
(11, 16, 'N'),
(11, 19, 'O'),
(12, 21, 'N'),
(12, 22, 'O'),
(13, 18, 'N'),
(13, 20, 'O'),
(14, 4, 'O'),
(14, 23, 'N'),
(15, 5, 'O'),
(15, 12, 'N'),
(16, 2, 'O'),
(17, 12, 'O'),
(17, 22, 'N'),
(18, 10, 'N'),
(18, 23, 'O'),
(19, 6, 'N'),
(19, 24, 'O'),
(20, 10, 'N'),
(20, 25, 'O'),
(21, 4, 'N'),
(21, 26, 'O'),
(22, 27, 'N'),
(22, 28, 'O'),
(23, 5, 'N'),
(23, 29, 'O');

-- --------------------------------------------------------

--
-- Structure de la table `joueur`
--

CREATE TABLE `joueur` (
  `id` int UNSIGNED NOT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `idUtilisateur` smallint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `joueur`
--

INSERT INTO `joueur` (`id`, `nom`, `prenom`, `idUtilisateur`) VALUES
(1, 'Michaud', 'Danielle', 1),
(2, 'Berube', 'Guerin', 11),
(3, 'Lacombe', 'Georges', 1),
(4, 'Boisclair', 'Latimer', 9),
(5, 'Margand', 'Victoire', 7),
(6, 'Labrecque', 'Catherine', 8),
(7, 'Guimond', 'Orlene', 1),
(8, 'Leblanc', 'Veronique', 6),
(9, 'Pinneau', 'Talbot', 1),
(10, 'Lemelin', 'Luc', 5),
(11, 'Algernon', 'Hetu', 1),
(12, 'Bazinet', 'Esmeraude', 12),
(15, 'Tisserand', 'Anna', 1),
(16, 'Senior', 'Marc', 13),
(17, 'Rousseau', 'Marine', 1),
(18, 'Riquier', 'Armand', 14),
(19, 'Dennis', 'Ormazd', 16),
(20, 'Beauchamps', 'Honore', 17),
(21, 'Smith', 'John', 10),
(22, 'Emond', 'Lance', 4),
(23, 'Doe', 'John', 15),
(24, 'Jenkins', 'Alfred', 19),
(25, 'Wilson', 'Leona', 20),
(26, 'Shires', 'Marie', 21),
(27, '', 'ohn-smith', 1),
(28, 'Patricia', 'Patricia', 22),
(29, 'Voisine', 'Gabrielle', 23);

-- --------------------------------------------------------

--
-- Structure de la table `rencontre`
--

CREATE TABLE `rencontre` (
  `idTournoi` smallint UNSIGNED NOT NULL,
  `horaire` datetime DEFAULT NULL,
  `idEquipe1` smallint UNSIGNED NOT NULL,
  `idEquipe2` smallint UNSIGNED NOT NULL,
  `score1` tinyint UNSIGNED DEFAULT NULL,
  `score2` tinyint UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `rencontre`
--

INSERT INTO `rencontre` (`idTournoi`, `horaire`, `idEquipe1`, `idEquipe2`, `score1`, `score2`) VALUES
(1, '2021-06-01 12:00:00', 1, 3, 5, 7),
(1, '2021-05-15 11:11:00', 2, 1, 4, 7),
(1, '2021-05-15 12:12:00', 3, 4, 6, 1),
(1, '2021-06-02 13:00:00', 3, 5, 6, 2),
(1, '2021-05-15 13:13:00', 5, 6, 11, 7),
(1, '2021-06-01 13:00:00', 5, 7, 10, 9),
(1, '2021-05-15 14:14:00', 7, 8, 23, 22),
(2, '2021-05-24 12:00:00', 4, 7, 14, 31),
(2, '2021-05-31 21:00:00', 7, 9, NULL, NULL),
(2, '2021-05-24 13:00:00', 9, 10, 9, 8),
(3, '2021-05-31 11:11:00', 5, 12, NULL, NULL),
(3, '2021-05-31 12:12:00', 13, 14, NULL, NULL),
(4, '2021-05-29 12:00:00', 20, 23, 69, 74),
(4, '2021-05-29 12:00:00', 21, 22, 1, 48),
(4, '2021-05-30 12:00:00', 22, 23, 44, 45),
(5, '2021-05-29 00:00:00', 18, 19, 74, 88),
(6, '2021-05-29 00:00:00', 8, 22, 86, 87),
(6, '2021-05-30 00:00:00', 12, 17, 50, 25),
(6, '2021-05-31 00:00:00', 12, 19, 91, 82),
(6, '2021-05-29 00:00:00', 12, 21, 78, 2),
(6, '2021-05-29 00:00:00', 15, 19, 51, 79),
(6, '2021-05-29 00:00:00', 16, 17, 32, 75),
(6, '2021-05-30 00:00:00', 19, 22, 74, 26);

-- --------------------------------------------------------

--
-- Structure de la table `tournoi`
--

CREATE TABLE `tournoi` (
  `id` smallint UNSIGNED NOT NULL,
  `nom` varchar(50) NOT NULL,
  `lieu` varchar(50) DEFAULT NULL,
  `dateDebut` datetime DEFAULT NULL,
  `dateFin` datetime DEFAULT NULL,
  `dureeHeure` int DEFAULT NULL,
  `dureeMinute` int DEFAULT NULL,
  `nbEquipe` tinyint UNSIGNED DEFAULT NULL,
  `specificite` char(1) NOT NULL DEFAULT 'T',
  `idGestionnaire` smallint UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `tournoi`
--

INSERT INTO `tournoi` (`id`, `nom`, `lieu`, `dateDebut`, `dateFin`, `dureeHeure`, `dureeMinute`, `nbEquipe`, `specificite`, `idGestionnaire`) VALUES
(1, 'Les Boulistes du Sud', 'Toulouse', '2021-05-12 00:00:00', '2021-06-28 23:59:00', 647, 59, 8, 'C', 10),
(2, 'Boulistes en Folie', 'Collioure', '2021-05-12 12:00:00', '2021-06-28 12:00:00', 1032, 0, 4, 'C', 15),
(3, 'Les meilleurs des meilleurs', 'Lille', '2021-05-14 12:00:00', '2021-06-22 14:00:00', 938, 0, 4, 'C', 15),
(4, 'Petit duel', 'Montpellier', '2021-05-28 12:00:00', '2021-06-30 12:00:00', 744, 0, 4, 'T', 10),
(5, 'Vrai duel', 'Paris', '2021-05-28 00:00:00', '2021-06-30 00:00:00', 744, 0, 2, 'T', 10),
(6, 'Huit à Huit', 'Montpellier', '2021-05-28 00:00:00', '2021-06-30 00:00:00', 744, 0, 8, 'T', 10);

-- --------------------------------------------------------

--
-- Structure de la table `tournoiequipe`
--

CREATE TABLE `tournoiequipe` (
  `idTournoi` smallint UNSIGNED NOT NULL,
  `idEquipe` smallint UNSIGNED NOT NULL,
  `Valide` char(1) DEFAULT 'N',
  `elimine` varchar(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `tournoiequipe`
--

INSERT INTO `tournoiequipe` (`idTournoi`, `idEquipe`, `Valide`, `elimine`) VALUES
(1, 1, 'O', 'O'),
(1, 2, 'O', 'O'),
(1, 3, 'O', 'N'),
(1, 4, 'O', 'O'),
(1, 5, 'O', 'O'),
(1, 6, 'O', 'O'),
(1, 7, 'O', 'O'),
(1, 8, 'O', 'O'),
(2, 4, 'O', 'O'),
(2, 7, 'O', 'N'),
(2, 9, 'O', 'N'),
(2, 10, 'O', 'O'),
(3, 5, 'O', 'N'),
(3, 12, 'O', 'N'),
(3, 13, 'O', 'N'),
(3, 14, 'O', 'N'),
(4, 20, 'O', 'O'),
(4, 21, 'O', 'O'),
(4, 22, 'O', 'O'),
(4, 23, 'O', 'N'),
(5, 18, 'O', 'O'),
(5, 19, 'O', 'N'),
(6, 8, 'O', 'O'),
(6, 12, 'O', 'N'),
(6, 15, 'O', 'O'),
(6, 16, 'O', 'O'),
(6, 17, 'O', 'O'),
(6, 19, 'O', 'O'),
(6, 21, 'O', 'O'),
(6, 22, 'O', 'O');

-- --------------------------------------------------------

--
-- Structure de la table `utilisateur`
--

CREATE TABLE `utilisateur` (
  `id` smallint UNSIGNED NOT NULL,
  `sexe` char(1) DEFAULT NULL,
  `dateNaissance` date DEFAULT NULL,
  `nom` varchar(30) NOT NULL,
  `prenom` varchar(30) NOT NULL,
  `pseudonyme` varchar(30) NOT NULL,
  `mail` varchar(50) NOT NULL,
  `motDePasse` varchar(32) NOT NULL,
  `gestionnaire` char(1) NOT NULL DEFAULT 'N',
  `administrateur` char(1) NOT NULL DEFAULT 'N'
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ;

--
-- Déchargement des données de la table `utilisateur`
--

INSERT INTO `utilisateur` (`id`, `sexe`, `dateNaissance`, `nom`, `prenom`, `pseudonyme`, `mail`, `motDePasse`, `gestionnaire`, `administrateur`) VALUES
(1, NULL, NULL, 'exterieur', 'exterieur', 'è-(465\'3(97^ù$', 'è-(465\'3(97^ù$', 'è-(465\'3(97^ù$', 'N', 'N'),
(2, 'F', '2021-05-12', 'administrateur', 'administrateur', 'admin', 'admin@admin.admin', '8380dd29d6538da2847dadafc65d645f', 'O', 'O'),
(3, 'F', '2021-05-12', 'utilisateur', 'utilisateur', 'user', 'user@user.user', '53588c0575c5cb4320d1af21c6b35f98', 'N', 'N'),
(4, 'F', '1981-11-15', 'Emond', 'Lance', 'Muctlented', 'LanceEmond@teleworm.us', 'e604a407e7e650c5f57fdb0456e943fc', 'N', 'N'),
(5, 'H', '1958-08-22', 'Lemelin', 'Luc', 'Thops1958', 'LucLemelin@teleworm.us', '5786e66a578851162bdd7e321cb14b4f', 'N', 'N'),
(6, 'H', '1960-07-13', 'Leblanc', 'Veronique', 'Wattelf', 'VeroniqueLeblanc@armyspy.com', '5a957daa995ec8216df7e22430cfde14', 'N', 'N'),
(7, 'F', '1977-12-09', 'Margand', 'Victoire', 'Wallut', 'VictoireMargand@teleworm.us', '9d054b0ba3836adf189a38110a39c9fc', 'N', 'N'),
(8, 'F', '1985-06-22', 'Labrecque', 'Catherine', 'Lorepatiou', 'CatherineLabrecque@armyspy.com', '70f103ef023bf992aee71b6a580c2134', 'N', 'N'),
(9, 'H', '1991-08-30', 'Boisclair', 'Latimer', 'Latimer', 'LatimerBoisclair@dayrep.com', '3a0cd4275033e5e2fcfd7ccaad2f2aeb', 'N', 'N'),
(10, 'H', '1977-09-09', 'Smith', 'John', 'john-smith', 'john-smith@pm.me', '65e598ec52d19d9e98f9d7e1b02809a9', 'O', 'N'),
(11, 'H', '1963-12-03', 'Berube', 'Guerin', 'berube', 'GuerinBerube@jourrapide.com', '81a4bbd7f9684b19b1770467664d95aa', 'N', 'N'),
(12, 'F', '1987-06-16', 'Bazinet', 'Esmeraude', 'Bazinet', 'bazinet-esmeraude@laposte.net', 'f64fa01577150a25bf7a4f959617aeb6', 'N', 'N'),
(13, 'H', '1963-05-05', 'Senior', 'Marc', 'Marc', 'marc-senoir@gmail.com', '1aa69c14cb5eba79efd79e0c88f1da5a', 'N', 'N'),
(14, 'H', '1988-06-01', 'Riquier', 'Armand', 'Armand', 'armand-riquier@laposte.net', '57137495903a04ccfa625da41e86a64f', 'N', 'N'),
(15, 'F', '1970-10-10', 'Doe', 'John', 'john-doe', 'john-doe@etu.umontpellier.fr', '0024f17f2eec60d2bda752839a80be67', 'O', 'N'),
(16, 'H', '1957-09-26', 'Dennis', 'Ormazd', 'Ormazd', 'ormazd@laposte.net', '76e43a2b6b6cde6a350d38f44dced9e9', 'N', 'N'),
(17, 'F', '1975-03-20', 'Beauchamps', 'Honore', 'Honore', 'honore-beauchamps@etu.umontpellire.fr', 'd48da286b68a42e925974603d6280bae', 'N', 'N'),
(19, 'H', '1977-05-22', 'Jenkins', 'Alfred', 'Alfred', 'alfred@yahoo.fr', 'f3a60254d24922cddd1029a7ecfb22fd', 'N', 'N'),
(20, 'F', '1997-05-18', 'Wilson', 'Leona', 'Leona', 'leona@monmail.fr', 'd72a7ed0b4c44d2339ed6ab497af2f61', 'N', 'N'),
(21, 'F', '1999-02-02', 'Shires', 'Marie', 'marie', 'marie@laforet.fr', '58cb8a6d38c224516437aa8b0b794475', 'N', 'N'),
(22, 'F', '2001-08-08', 'Patricia', 'Patricia', 'patricia', 'patricia@posteo.com', '0dc3c126e98b72ab366316c6ac53f803', 'N', 'N'),
(23, 'F', '1996-08-28', 'Voisine', 'Gabrielle', 'voisine', 'g.voisine@gmail.com', '10002b1237d4ee27b4808e62a44d888c', 'N', 'N');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `equipe`
--
ALTER TABLE `equipe`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `equipejoueur`
--
ALTER TABLE `equipejoueur`
  ADD PRIMARY KEY (`idEquipe`,`idJoueur`),
  ADD KEY `idJoueur` (`idJoueur`);

--
-- Index pour la table `joueur`
--
ALTER TABLE `joueur`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idUtilisateur` (`idUtilisateur`);

--
-- Index pour la table `rencontre`
--
ALTER TABLE `rencontre`
  ADD PRIMARY KEY (`idTournoi`,`idEquipe1`,`idEquipe2`),
  ADD KEY `idEquipe1` (`idEquipe1`),
  ADD KEY `idEquipe2` (`idEquipe2`);

--
-- Index pour la table `tournoi`
--
ALTER TABLE `tournoi`
  ADD PRIMARY KEY (`id`),
  ADD KEY `idGestionnaire` (`idGestionnaire`);

--
-- Index pour la table `tournoiequipe`
--
ALTER TABLE `tournoiequipe`
  ADD PRIMARY KEY (`idTournoi`,`idEquipe`),
  ADD KEY `idEquipe` (`idEquipe`);

--
-- Index pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `indPseudonyme` (`pseudonyme`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `equipe`
--
ALTER TABLE `equipe`
  MODIFY `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT pour la table `joueur`
--
ALTER TABLE `joueur`
  MODIFY `id` int UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT pour la table `tournoi`
--
ALTER TABLE `tournoi`
  MODIFY `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `utilisateur`
--
ALTER TABLE `utilisateur`
  MODIFY `id` smallint UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- Contraintes pour les tables déchargées
--

--
-- Contraintes pour la table `equipejoueur`
--
ALTER TABLE `equipejoueur`
  ADD CONSTRAINT `equipejoueur_ibfk_1` FOREIGN KEY (`idEquipe`) REFERENCES `equipe` (`id`),
  ADD CONSTRAINT `equipejoueur_ibfk_2` FOREIGN KEY (`idJoueur`) REFERENCES `joueur` (`id`);

--
-- Contraintes pour la table `joueur`
--
ALTER TABLE `joueur`
  ADD CONSTRAINT `joueur_ibfk_1` FOREIGN KEY (`idUtilisateur`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `rencontre`
--
ALTER TABLE `rencontre`
  ADD CONSTRAINT `rencontre_ibfk_1` FOREIGN KEY (`idTournoi`) REFERENCES `tournoi` (`id`),
  ADD CONSTRAINT `rencontre_ibfk_2` FOREIGN KEY (`idEquipe1`) REFERENCES `equipe` (`id`),
  ADD CONSTRAINT `rencontre_ibfk_3` FOREIGN KEY (`idEquipe2`) REFERENCES `equipe` (`id`);

--
-- Contraintes pour la table `tournoi`
--
ALTER TABLE `tournoi`
  ADD CONSTRAINT `tournoi_ibfk_1` FOREIGN KEY (`idGestionnaire`) REFERENCES `utilisateur` (`id`);

--
-- Contraintes pour la table `tournoiequipe`
--
ALTER TABLE `tournoiequipe`
  ADD CONSTRAINT `tournoiequipe_ibfk_1` FOREIGN KEY (`idTournoi`) REFERENCES `tournoi` (`id`),
  ADD CONSTRAINT `tournoiequipe_ibfk_2` FOREIGN KEY (`idEquipe`) REFERENCES `equipe` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
