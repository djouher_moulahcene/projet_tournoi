<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <link href="style.css" rel="stylesheet" type="text/css" />
    <title>Connexion</title>
</head>

<body>

    <?php 
        session_start();

        if(isset($_SESSION['pseudonyme']))
            header('Location: espace_membre.php');
        else{
    ?>

    <div class="base" style="margin:auto;margin-top:50px;">
            <!-- zone de connexion -->
        <form action="verification_connexion.php" method="POST">
            <h1>Connexion</h1>

            <!-- message si mdp ou id incorrect -->
            <?php
            if(isset($_GET['erreur'])){
                $err = $_GET['erreur'];
                if($err==1 || $err==2)
                echo "<div style='background:#F0AFAF; border: 2px solid red;font-weight: bold;'><p style='text-align: center;'>Utilisateur ou mot de passe incorrect</p></div></br>";
            }

            if(isset($_GET['complete'])){
                $com = $_GET['complete'];
                if($com==0)
                    echo "<div style='background:#C5F7CA; border: 2px solid green;font-weight: bold;'><p style='text-align: center;'>Inscription complète</p></div></br>";
            }

            // Message de déconnexion
            if(isset($_GET['logout'])){
                $logout = $_GET['logout'];
                if($logout==0)
                    echo "<div style='background:#C5F7CA; border: 2px solid green;font-weight: bold;'><p style='text-align: center;'>Vous êtes déconnecté</p></div></br>";
            }
            ?>

            <!-- Formulaire de connexion   -->
            <label title="Username / surnom / pseudonyme"><b>Nom d'utilisateur</b></label>
            <input type="text" placeholder="Entrer le nom d'utilisateur" name="pseudonyme" required>

            <label title="Password"><b>Mot de passe</b></label>
            <input type="password" placeholder="Entrer le mot de passe" name="motDePasse" required>

            <input type="submit" id='submit' value='Se connecter'>
            
            <a href=inscription.php style="color:black;font-style: italic;text-decoration: underline">Pas encore inscrit ? S'inscrire</a>
            </br></br><center><a href=accueil.php style="color:black;font-style: bold;text-decoration: underline">Retour à la page d'acceuil</a></center>
        </form>
    </div>

    <?php } ?>

</body>

</html>