<?php 

    require_once 'bdd/bdd.php';

    if($_SESSION['pseudonyme'] != ""){ //vérification si utilisateur connecté
        include ('parts/header_logged.php');

        echo '</br></br>';

        //bouton retour arrière vers la liste des compétitions
        echo "<center><table style='width:65%;'><tr><td><a href='page_equipe.php'>← liste des équipes</a></td></tr></table></center>";

        $idEquipe=$_GET['id'];
        $_SESSION['idEquipe']=$idEquipe;
        $idUtilisateur=$_SESSION['id'];
        $admin=$_SESSION['admin'];

        $requete = "SELECT * FROM equipe WHERE id = '".$idEquipe."'";
        $exec_requete = mysqli_query($db,$requete);
        $row = mysqli_fetch_array($exec_requete);

        $nom=$row['nom'];
        $mail=$row['mail'];
        $telephone=$row['telephone'];
        $nbCompet=$row['nbCompet'];

        $requete = "SELECT idJoueur FROM equipejoueur WHERE idEquipe = '".$idEquipe."' AND capitaine = 'O'";
        $exec_requete = mysqli_query($db,$requete);
        $reponse = mysqli_fetch_array($exec_requete);
        $idCapitaine = $reponse['idJoueur'];

        $requete = "SELECT * FROM joueur WHERE id = '".$idCapitaine."'";
        $exec_requete = mysqli_query($db,$requete);
        $reponse = mysqli_fetch_array($exec_requete);
        $prenomCapitaine = $reponse['prenom'];
        $nomCapitaine = $reponse['nom'];
        $idUtilisateurCapitaine = $reponse['idUtilisateur'];

        include ('parts/fiche_equipe.php');

        include ('includes/footer.php');
        
        if(isset($_GET['complete'])){
            $complete = $_GET['complete'];
            if($complete==1){
            $message='Demande envoyée';
            echo '<script type="text/javascript">window.alert("'.$message.'");</script>';
            }
        }
    }
    else
        header('Location: connexion.php');
?>