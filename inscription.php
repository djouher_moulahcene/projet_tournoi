<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <link href="style.css" rel="stylesheet" type="text/css" />
    <title>Inscription</title>
</head>

<body>

    <?php 
        session_start();

        if(isset($_SESSION['pseudonyme']))
            header('Location: espace_membre.php');
        else{
    ?>

    <div class="base" style="margin:auto;">
            <!-- zone d'inscription' -->
        <form action="confirmation_inscription.php" method="POST">
            <h1>Inscription</h1>

            <a href=connexion.php style="color:black;font-style: italic;text-decoration: underline" title="Retour à la page de conexion">← Connexion</a></br></br>

            <!-- Message si email ou pseudo déjà utilisé -->
            <?php
            if(isset($_GET['erreur'])){
                $err = $_GET['erreur'];
                if($err==1)
                echo "<div style='background:#F0AFAF; border: 2px solid red;font-weight: bold;'><p style='text-align: center;'>Pseudo déjà utilisé</p></div></br>";
                else if($err==2)
                echo "<div style='background:#F0AFAF; border: 2px solid red;font-weight: bold;'><p style='text-align: center;'>Adresse mail déjà utilisé</p></div></br>";
            }
            ?>

            <!-- Formulaire d'inscription -->
            <label title="First Name / Nom de famille"><b>Nom</b></label>
            <input type="text" minlength="1" maxlength="28" placeholder="Entrer votre nom" name="nom" required>

            <label title="Last name"><b>Prenom</b></label>
            <input type="text" minlength="1" maxlength="28" placeholder="Entrer votre prenom" name="prenom" required>

            <label title="Genre"><b>Sexe</b></label>
            <p><input type="radio" name="sexe" value="H" style="display:inline;">Homme</input>
            <input type="radio" name="sexe" value="F" style="display:inline;margin-left:50px;" checked>Femme</input></p>

            <label title="Date of birth"><b>Date de naissance</b></label>
            <input type="date" name="dateNaissance" required>

            <label title="Email"><b>Adresse mail</b></label>
            <input type="email" placeholder="Entrer votre adresse mail" name="mail" required>
                
            <label title="Username / Surnom / Pseudo"><b>Nom d'utilisateur</b></label>
            <input type="text" minlength="1" maxlength="28" placeholder="Entrer le nom d'utilisateur" name="pseudonyme" required>

            <label title="Password"><b>Mot de passe</b></label>
            <br />
            <label> 

              <input type="password" minlength="1" maxlength="28" placeholder="Entrer le mot de passe" name="motDePasse" required>
             <div class="password-icon">
               <i data-feather="eye"></i>
               <i data-feather="eye-off"></i>
             </div>
          </label>
          <!-- Icon script-->
          <!-- c'est une librarie d'icones -->
          <script src="https://unpkg.com/feather-icons"></script>
          <script> 
           feather.replace();
           </script>

           <script>
              const eye = document.querySelector('.feather-eye');
              const eyeoff = document.querySelector('.feather-eye-off');
              //pour modifier le type de passeword dans input en texte
              const passwordField = document.querySelector('input[type=password]');
              eye.addEventListener('click', () => {
              eye.style.display = "none"; 
              eyeoff.style.display ="block"; 
              passwordField.type="text";
               });
              eyeoff.addEventListener('click', () => {
              eyeoff.style.display = "none"; 
              eye.style.display ="block"; 
              passwordField.type="password";
               });
            </script>

            <input type="submit" id='submit' value='INSCRIPTION'>

            </br></br><center><a href=accueil.php style="color:black;font-style: bold;text-decoration: underline">Retour à la page d'acceuil</a></center>
        </form>
    </div>

    <?php } ?>
</body>

</html>
