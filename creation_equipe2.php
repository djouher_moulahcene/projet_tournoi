<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8" />
    <link href="style.css" rel="stylesheet" type="text/css" />
    <title>Création d'équipe</title>
</head>

<body>

    <?php 
        session_start();

        if(!isset($_SESSION['pseudonyme']))
            header('Location: connexion.php');
        else{
    ?>

        </br>
        <!-- formulaire de création de tournoi -->
        <div class="base" style="margin:auto">

        <form action="confirmation_equipe.php" method="POST">
            <h1>Créer une equipe <a href='#' class='bulle' style='bottom:15px'><img src=images/infobulles.png style='width:20px;'><span>Inscrire un joueur en indiquant soit son <u>pseudonyme Tournoi x Tournoi</u> <b>soit</b> en indiquant manuellement son <u>nom et son prenom</u></span> </a></b></center></h1>
                
            <?php
            if(isset($_GET['erreur'])){
                $err = $_GET['erreur'];
                if($err==1)
                    echo "<div style='background:#F0AFAF; border: 2px solid red;font-weight: bold;'><p style='text-align:center;margin-top:10px;'>Un pseudonyme est introuvable</p></div></br>";
            }
            ?>

            <label><b>Nom : </b></label>
            <?php

            if( (isset($_POST['nomEquipe']) && isset($_POST['mail']) && isset($_POST['telephone']) && isset($_POST['nbJoueur'])) ) {
                $nomEquipe = $_POST['nomEquipe'];
                $_SESSION['nomEquipe'] = $nomEquipe;
                $mail=$_POST['mail'];
                $_SESSION['mail'] = $mail;
                $telephone=$_POST['telephone'];
                $_SESSION['telephone'] = $telephone;
                $nbJoueur=$_POST['nbJoueur'];
                $_SESSION['nbJoueur'] = $nbJoueur;
            }
            else if( (isset($_SESSION['nomEquipe']) && isset($_SESSION['mail']) && isset($_SESSION['telephone']) && isset($_SESSION['nbJoueur'])) ) {
                $nomEquipe = $_SESSION['nomEquipe'];
                $mail = $_SESSION['mail'];
                $telephone = $_SESSION['telephone'];
                $nbJoueur = $_SESSION['nbJoueur'];
            }

            echo "<b name='nomEquipe'>"; echo $_SESSION['nomEquipe']; echo"</b>"; ?>

            </br></br>

            <label><b>Nombre de joueur : </b></label>
            <?php echo "<b name='nbJoueur'>"; echo $_SESSION['nbJoueur']; echo"</b></br></br>";?>

            <label><b>Mail : </b></label>
            <?php echo "<b name='mail'>"; echo $_SESSION['mail']; echo"</b></br></br>";?>

            <label><b>Téléphone : </b></label>
            <?php echo "<b name='telephone'>"; echo $_SESSION['telephone']; echo"</b></br></br>"; ?></br>

            <table style="border:3px solid;border-collapse:collapse;width:100%;">
            <tr style="border:3px solid;"><td style="padding-top:12px;padding-bottom:12px;padding-left:12px;">
            <lable><b>Joueur 1 (capitaine)</b></label></br></br>
            <?php 
            
                $nom = $_SESSION['nom'];
                $prenom = $_SESSION['prenom'];

                echo "$nom $prenom</br></td></tr>";

                for($i=1;$i<=$nbJoueur-1;$i++){
                  $a=$i+1;
                  
                  $chaine = "joueurName";
                  $chaine2 = $chaine."".$i;

                  if(isset($_SESSION[$chaine2])) 
                   $joueur = $_SESSION[$chaine2];
                  else
                   $joueur = "";

                  echo "
                  <tr style='border:3px solid;'><td style='padding-top:12px;padding-bottom:12px;padding-left:12px;'>
                  <b>Joueur $a</b></br>
                  <input type='radio' id='pseudo' name='joueur$i' value='pseudo' checked>
                  <label for='pseudo'>Inscrire via pseudo</label><br>
                  <input type='radio' id='manuellement' name='joueur$i' value='manuellement'>
                  <label for='manuellement'>Inscrire manuellement (Nom + Prénom)</label><br>

                  <input style='width:98%;' type='text' minlength='1' maxlength='28'  name='joueurName$i' value='$joueur' required>
                  
                  </td></tr>
                  ";
                }

                echo "
                </table></br>
                <input type='submit' id='submit' value='Créer une équipe'>

                </br>

                <a href=creation_equipe1.php style='color:black;'>Annuler</a>

                </form>
                </div>
                </br>";
              
              } 
              ?>

</body>
</html>