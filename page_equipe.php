<?php 

    require_once 'bdd/bdd.php';

    if($_SESSION['pseudonyme'] != ""){ //vérification si utilisateur connecté
        $title = 'Equipes'; 
        $page = 'page_equipe';

        include ('parts/header_logged.php');

        echo "<div class='allButFooter'>";

        echo '</br></br>';

        if(isset($_GET['complete'])){
            $com = $_GET['complete'];
            if($com==0)
            echo "<center><div style='width:70%;background:#C5F7CA; border: 2px solid green;font-weight: bold;'><h1 style='text-align: center;'>Equipe créée</h1></div></br></center>";
        }

        echo "
            <center>
            <b style='font-size:35px;color:white;'>Vos équipes <a href='#' class='bulle' style='bottom:15px'><img src=images/infobulles.png style='width:20px;'><span>Liste des équipes dont vous êtes joueur</span> </a></b></center>
            <b style='margin-left:15%;color:white'>* équipes dont je suis le capitaine</b>";

        include ('parts/affichage_equipe.php');

        echo "<a href=creation_equipe1.php style='margin-left:15%;'>+ Créer une équipe</a></br>";

        echo "<center><b style='font-size:35px;color:white;'>Toutes les équipes <a href='#' class='bulle' style='bottom:15px'><img src=images/infobulles.png style='width:20px;'><span>Liste de toutes les équipes inscrites sur le site actuellement</span> </a></b></center></br>";

        include ('parts/affichageALL_equipe.php');

        echo "</div>";

        include 'includes/footer.php';

    }
    else
        header('Location: connexion.php');

?>