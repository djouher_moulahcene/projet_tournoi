    <footer>
		<div class="container">
			<div class="row text-center py-3">
			<div class="col-md-4">
				<img class="mb-1" src="images/location.png" alt="">
				<p class="">34000 Montpellier</p>
				</div>
			
				<div class="col-md-4">
				<img class="mb-1" src="images/email.png" alt="">
				<p>info@tournoixtournoi.fr</p>
				</div>
				
				<div class="col-md-4">
					<img class="mb-1" src="images/phone.png" alt="">
					<p>06-06-06-06-06</p>
				</div>
			</div><!--- End of Row -->

			<div class="row text-center mt-2">
				<div class="col-md-12"> 
				<p class="">Page faite avec amour par: Xubuntu</p>
				
				</div>
			</div>

			<div class="row text-center">
				<div class="col-md-12"> 
					<img class="mr-2" src="images/fb1.png" alt="">
					<img class="" src="images/insta1.png" alt="">
				</div>
			</div>

			<div class="row text-center mt-2">
			</div>
		</div><!--- End of Container -->
	</footer>

    <!--les scripts du bootstrap4.6-->
    <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js" integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js" integrity="sha384-Piv4xVNRyMGpqkS2by6br4gNJ7DXjqk09RmUpJ8jgGtD7zP9yug3goQfGII0yAns" crossorigin="anonymous"></script>
</body>
</html>