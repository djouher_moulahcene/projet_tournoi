<?php

    require_once 'bdd/bdd.php';

    $nbRencontre = $_SESSION['nbRencontreRestant'];
    $idTournoi = $_GET['id'];

    $i=0;
    $j=1;

    while ($i<2*$nbRencontre) {

        $chaineName1 = "equipe1Rencontre";
        $equipe1C = $chaineName1."".$j;

        $chaineName2 = "equipe2Rencontre";
        $equipe2C = $chaineName2."".$j;

        $equipe1 = $_POST[$equipe1C];
        $equipe2 = $_POST[$equipe2C];

        $id[$i] = $equipe1;
        $id[$i+1] = $equipe2;

        if($id[$i]=="vide" || $id[$i+1]=="vide")
          $bool=1;

        $i=$i+2;
        $j++;
    }

  if($bool){
      header("Location:tournoi_onclick.php?id=".$idTournoi."&erreur=2#confirmation");
  }

  else{

    $bool=0;
    $boolDate=0;

    for ($i = 0; $i < $nbRencontre-1; $i++) {
        for ($j = $i + 1 ; $j < $nbRencontre; $j++) {
          if ($id[$i] == $id[$j]) {
            $bool = 1;
          }
        }
    }

    if($bool){
        header("Location:tournoi_onclick.php?id=".$idTournoi."&erreur=1#confirmation");
    }
    else{
      $i=1;
      $erreurDate=0;
      $date = date('Y-m-d H:i');

      while($i<=$nbRencontre){
        $chaine = "dateRencontre";
        $chaineId = $chaine."".$i;
        $dateRencontre = $_POST[$chaineId];

        $chaine = "heureRencontre";
        $chaineId = $chaine."".$i;
        $heureRencontre = $_POST[$chaineId];

        $dateHeureRencontre[$i] = $dateRencontre." ".$heureRencontre;

        echo "$date / $dateHeureRencontre[$i] </br>";

        if($dateHeureRencontre[$i] < $date)
          $erreurDate = 1;

        $i++;
      }

      $i=0;
      $j=1;

      if($erreurDate != 0){
        header("Location:tournoi_onclick.php?id=".$idTournoi."&erreur=3#confirmation");
      }
      else{
      while($i<(2*$nbRencontre)){
        $idEquipe1 = $id[$i];
        $idEquipe2 = $id[$i+1];
        $dateHeure = $dateHeureRencontre[$j];

        $bdd -> query("INSERT INTO rencontre VALUES ($idTournoi,'".$dateHeure."',$idEquipe1,$idEquipe2,NULL,NULL)");

        $i=$i+2;
        $j++;
      }
      header("Location:tournoi_onclick.php?id=".$idTournoi."&complete=1");
      }
    }
  }
?>