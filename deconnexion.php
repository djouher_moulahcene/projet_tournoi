<?php 
    session_start();

    // Suppression des variables de session et de la session
    $_SESSION = array();
    session_destroy();
    
    //redirection vers la page de connexion
    header('Location: connexion.php?logout=0');
?>