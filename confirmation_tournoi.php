<?php 
    session_start();
    ini_set('display_errors', "stderr");error_reporting(E_ALL);

    // connexion à la base de données
    $db_username = 'root';
    $db_password = '';
    $db_name     = 'gestiontournoi';
    $db_host     = 'gestiontournoi';
    $db = mysqli_connect($db_host, $db_username, $db_password,$db_name)
           or die('could not connect to database');


    $pseudoGestionnaire = $_POST['pseudoGestionnaire'];
    $_SESSION['pseudoGestionnaire'] = $pseudoGestionnaire;
    $nbEquipe = $_POST['nbEquipe'];
    $_SESSION['nbEquipe'] = $nbEquipe;
    $nomTournoi = $_POST['nomTournoi'];
    $_SESSION['nomTournoi'] = $nomTournoi;
    $lieu = $_POST['lieu'];
    $_SESSION['lieu'] = $lieu;
    $dateDebutTournoi = $_POST['dateDebutTournoi'];
    $_SESSION['dateDebutTournoi'] = $dateDebutTournoi;
    $heureDebutTournoi = $_POST['heureDebutTournoi'];
    $_SESSION['heureDebutTournoi'] = $heureDebutTournoi;
    $dateFinTournoi = $_POST['dateFinTournoi'];
    $_SESSION['dateFinTournoi'] = $dateFinTournoi;
    $heureFinTournoi = $_POST['heureFinTournoi'];
    $_SESSION['heureFinTournoi'] = $heureFinTournoi;
    $specificite = $_POST['specificite'];
    $_SESSION['specificite'] = $specificite;

    $requeteCountId = "SELECT count(*) FROM utilisateur WHERE pseudonyme = '".$pseudoGestionnaire."'";
    $requeteId = "SELECT id FROM utilisateur WHERE pseudonyme = '".$pseudoGestionnaire."'";

    $exec_requeteCountId = mysqli_query($db,$requeteCountId);
    $reponseCountId      = mysqli_fetch_array($exec_requeteCountId);
    $count               = $reponseCountId['count(*)'];

    if( ($dateDebutTournoi > $dateFinTournoi) || ($dateDebutTournoi == $dateFinTournoi) && ($heureDebutTournoi >= $heureFinTournoi) )
        header('location:parts/creation_tournoi.php?erreur=3');
    else {

    if($count!=0){
        $puiss = 2;
        $bool = 0;
        while ($puiss < 65) {
            if ($nbEquipe == $puiss) {
                $bool = 1;
            }
            $puiss = $puiss*2;
        }

        if($bool==0)
            header('location:parts/creation_tournoi.php?erreur=2');
        else{
            $exec_requeteId = mysqli_query($db,$requeteId);
            $reponseId      = mysqli_fetch_array($exec_requeteId);
            $idGestionnaire = $reponseId['id'];

            $requeteG = "UPDATE utilisateur SET gestionnaire='O' WHERE id= '".$idGestionnaire."'";
            mysqli_query($db,$requeteG);

            $dateHeureDebut = $dateDebutTournoi." ".$heureDebutTournoi;
            $dateHeureFin = $dateFinTournoi." ".$heureFinTournoi;

            //durée de la compétion, résultat en secondes
            $diff = abs(strtotime($dateHeureFin) - strtotime($dateHeureDebut));

            //conversion de la durée en heures et minutes
            $dureeHeure = floor($diff/3600);
            $dureeMinute = ($diff - $dureeHeure * 3600) / 60;



            //insertion des infos formulaires dans la bdd
            $requete = "INSERT INTO tournoi VALUES (NULL,'".$nomTournoi."', '".$lieu."', '".$dateHeureDebut."', '".$dateHeureFin."','".$dureeHeure."', '".$dureeMinute."', '".$nbEquipe."', '".$specificite."', '".$idGestionnaire."')";
            $exec_requete = mysqli_query($db,$requete);
            $reponse      = mysqli_fetch_array($exec_requete);

            header('Location: tournoi.php?complete=0');
        }
    }
    else
        header('location:parts/creation_tournoi.php?erreur=1');
}

    mysqli_close($db); // fermer la connexion
?>