<?php 
    require_once 'bdd/bdd.php';

    if($_SESSION['pseudonyme'] != ""){ //affichage espace membre connecté
        $title = 'Espace membre';
        $page = 'espace_membre';
        include ('parts/header_logged.php');

        echo '</br></br>';

        include ('parts/information_membre.php');
    }
    else //redirection vers la page de connexion sinon
        header('Location: connexion.php');
?>