<?php 
    session_start();
    ini_set('display_errors', "stderr");error_reporting(E_ALL); 

    try
    {
        $bdd = new PDO('mysql:host=localhost;dbname=gestiontournoi;charset=utf8', 'root', '');
    }
    catch(Exception $e)
    {
        die('Erreur : '.$e->getMessage());
    }

    if($_SESSION['pseudonyme'] != ""){ //affichage espace membre connecté
        $title = 'Valider une equipe'; 
        $page = 'validation_tournoi';
        include ('parts/header_logged.php');

        echo '</br></br>';

        include ('parts/affichage_validation.php');
    }
    else //redirection vers la page de connexion sinon
        header('Location: connexion.php');
?>