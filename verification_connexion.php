<?php

if(isset($_POST['pseudonyme']) && isset($_POST['motDePasse']))
{
    require_once 'bdd/bdd.php';
    
    // on applique les deux fonctions mysqli_real_escape_string et htmlspecialchars
    // pour éliminer toute attaque de type injection SQL et XSS
    $pseudonyme = mysqli_real_escape_string($db,htmlspecialchars($_POST['pseudonyme'])); 
    $motDePasse = mysqli_real_escape_string($db,htmlspecialchars($_POST['motDePasse']));
    $salt = "SOURIS";
    $motDePasse = $motDePasse . $salt;
    $motDePasse = md5($motDePasse);
    
    if($pseudonyme !== "" && $motDePasse !== ""){
      $requete = "SELECT count(*) FROM utilisateur WHERE pseudonyme = '".$pseudonyme."' AND motDePasse = '".$motDePasse."' ";
      $exec_requete = mysqli_query($db,$requete);
      $reponse      = mysqli_fetch_array($exec_requete);
      $count        = $reponse['count(*)'];
      if($count!=0) // nom d'utilisateur et mot de passe correctes
      {

         $requete = "SELECT * FROM utilisateur WHERE pseudonyme = '".$pseudonyme."'";
         $exec_requete = mysqli_query($db,$requete);
         $row = mysqli_fetch_array($exec_requete);

         $prenom = $row['prenom'];
         $nom = $row['nom'];
         $sexe = $row['sexe'];
         $dateNaissance = $row['dateNaissance'];
         $mail = $row['mail'];
         $gestionnaire = $row['gestionnaire'];
         $id = $row['id'];
         $admin = $row['administrateur'];

         $_SESSION['pseudonyme'] = $pseudonyme;
         $_SESSION['motDePasse'] = $motDePasse;
         $_SESSION['prenom'] = $prenom;
         $_SESSION['nom'] = $nom;
         $_SESSION['sexe'] = $sexe;
         $_SESSION['dateNaissance'] = $dateNaissance;
         $_SESSION['mail'] = $mail;
         $_SESSION['gestionnaire'] = $gestionnaire;
         $_SESSION['id'] = $id;
         $_SESSION['admin'] = $admin;

         header('Location: tournoi.php');
      }
      else{
         header('Location: connexion.php?erreur=1'); // utilisateur ou mot de passe incorrect
      }
    }
    else{
         header('Location: connexion.php?erreur=2'); // utilisateur ou mot de passe vide
    }
}
else
{
   header('Location: connexion.php');
}
mysqli_close($db); // fermer la connexion
?>
